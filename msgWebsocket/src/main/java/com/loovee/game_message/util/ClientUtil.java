package com.loovee.game_message.util;

import com.loovee.game.im.ImClient;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/15
 */
public class ClientUtil {

    public static void sendMsg(String host, int port, String msg){
        ImClient client = new ImClient(host, port);
        client.sendMsg(msg);
    }

}
