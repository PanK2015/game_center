package com.loovee.game_message.util;

import com.loovee.game_message.handle.GameWebsocketHandler;

import java.util.Set;

/**
 * Created by loovee on 2016/9/29.
 */
public class SendMsgToAll implements Runnable
{
    private String msg;

    private Set<String> user_channels;

    public SendMsgToAll(String msg, Set<String> set)
    {
        this.msg = msg;
        this.user_channels = set;
    }

    @Override
    public void run()
    {

        for (String sessionId : user_channels)
        {
            GameWebsocketHandler.sendToSession(sessionId, msg);
        }

    }
}
