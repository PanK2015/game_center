package com.loovee.game_message;

import com.alibaba.druid.pool.DruidDataSource;
import com.loovee.game_message.config.DataSource;
import com.loovee.redis.connection.RedisConnection;
import com.xxx.rpc.client.RpcProxy;
import com.xxx.rpc.registry.zookeeper.ZooKeeperServiceDiscovery;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.server.standard.ServletServerContainerFactoryBean;

import java.io.IOException;

@SpringBootApplication(scanBasePackages = {"com.loovee.game_message.config", "com.loovee.game_message.jms", "com.loovee.game_message.handle", "com.loovee.game_message.controller"})
@EnableWebSocket
public class MsgWebsocketApplication {

    private static Logger log = LoggerFactory.getLogger(MsgWebsocketApplication.class);

    @Value("${redis.hosts}")
    String host;
    @Value("${redis.maxTotal}")
    int maxTotal;
    @Value("${redis.maxWaitMillis}")
    long maxWaitMillis;
    @Value("${redis.maxIdle}")
    int maxIdle;
    @Value("${redis.testOnBorrow}")
    Boolean testOnBorrow;

    @Value("${rpc.registry_address}")
    String rpc_regAddr;

    @Autowired
    DataSource dataSource;

    /**
     * 获取redis链接
     *
     * @return
     */
    @Bean
    RedisConnection redisConnection() {
        RedisConnection connection = null;
        try {
            connection = new RedisConnection(host, maxTotal, maxWaitMillis, maxIdle, testOnBorrow);
        } catch (Exception e) {
            log.error("", e);
        }
        return connection;
    }

    @Bean
    public DruidDataSource druidDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(dataSource.getDriverClassName());
        druidDataSource.setUrl(dataSource.getUrl());
        druidDataSource.setUsername(dataSource.getUsername());
        druidDataSource.setPassword(dataSource.getPassword());
        druidDataSource.setInitialSize(dataSource.getInitialSize());
        druidDataSource.setMaxActive(dataSource.getMaxActive());
        druidDataSource.setMaxWait(dataSource.getMaxWait());
        druidDataSource.setTimeBetweenEvictionRunsMillis(dataSource.getTimeBetweenEvictionRunsMillis());
        druidDataSource.setMinEvictableIdleTimeMillis(dataSource.getMinEvictableIdleTimeMillis());
        druidDataSource.setValidationQuery(dataSource.getValidationQuery());
        druidDataSource.setTestWhileIdle(dataSource.isTestWhileIdle());
        druidDataSource.setTestOnBorrow(dataSource.isTestOnBorrow());
        druidDataSource.setTestOnReturn(dataSource.isTestOnReturn());
        druidDataSource.setPoolPreparedStatements(dataSource.isPoolPreparedStatements());
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(
                dataSource.getMaxPoolPreparedStatementPerConnectionSize());
        druidDataSource.setRemoveAbandonedTimeout(dataSource.getRemoveAbandonedTimeout());
        druidDataSource.setRemoveAbandoned(dataSource.isRemoveAbandoned());
        return druidDataSource;

    }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() {

        return new DataSourceTransactionManager(druidDataSource());

    }

    @Bean
    TransactionTemplate transactionTemplate() {
        TransactionTemplate template = new TransactionTemplate(dataSourceTransactionManager());
        template.setIsolationLevelName("ISOLATION_DEFAULT");
        template.setPropagationBehaviorName("PROPAGATION_REQUIRED");
        return template;

    }

    @Bean
    SqlSessionFactoryBean sqlSessionFactoryBean() {
        SqlSessionFactoryBean session = new SqlSessionFactoryBean();
        session.setDataSource(druidDataSource());
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            Resource[] resources = resolver.getResources("classpath*:/mybatis/mapper/*.xml");
            session.setMapperLocations(resources);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return session;

    }

    /**
     * 获得数据库连接
     *
     * @return
     * @throws Exception
     */
    @Bean
    SqlSessionTemplate sqlSessionTemplate() throws Exception {

        return new SqlSessionTemplate(sqlSessionFactoryBean().getObject());

    }

    @Bean
    public ServletServerContainerFactoryBean createWebSocketContainer() {
        ServletServerContainerFactoryBean container = new ServletServerContainerFactoryBean();
        container.setMaxTextMessageBufferSize(8192);
        container.setMaxBinaryMessageBufferSize(8192);
        container.setMaxSessionIdleTimeout(300000);
        return container;
    }

    @Bean
    ZooKeeperServiceDiscovery zooKeeperServiceDiscovery()
    {
        return new ZooKeeperServiceDiscovery(rpc_regAddr);
    }

    @Bean
    RpcProxy rpcProxy()
    {
        return new RpcProxy(zooKeeperServiceDiscovery());
    }

    public static void main(String[] args) {
        SpringApplication.run(MsgWebsocketApplication.class, args);
    }
}
