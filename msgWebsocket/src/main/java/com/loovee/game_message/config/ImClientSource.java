package com.loovee.game_message.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * Created by china on 2016/9/30.
 */
@Component
@ConfigurationProperties(prefix = "im", locations = "classpath:imClient.properties")
public class ImClientSource {

    private String host;

    private int port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }
}
