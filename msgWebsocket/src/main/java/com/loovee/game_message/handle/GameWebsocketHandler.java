package com.loovee.game_message.handle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loovee.game.consts.RoomConst;
import com.loovee.game.consts.UserConst;
import com.loovee.game.message.bean.ChannelSession;
import com.loovee.game.message.bean.GameWebSocketSession;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.enums.iq.ControllerEnum;
import com.loovee.game.protocol.enums.message.MessageTypeEnum;
import com.loovee.game.protocol.enums.stream.OperationEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game_message.config.ImClientSource;
import com.loovee.game_message.config.SpringContextUtil;
import com.loovee.game_message.controller.BaseControlller;
import com.loovee.game_message.jms.AmqQueueSender;
import com.loovee.game_message.util.ClientUtil;
import com.loovee.game_message.util.NetAddressUtil;
import com.loovee.game_message.util.SendMsgToAll;
import com.loovee.game_message.util.ThreadPoolManager;
import com.loovee.redis.connection.RedisConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;
import redis.clients.jedis.ShardedJedis;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/11
 */
@Component
public class GameWebsocketHandler extends TextWebSocketHandler {

    private static Logger log = LoggerFactory.getLogger(GameWebsocketHandler.class);

    public static Map<String, GameWebSocketSession> clients = new ConcurrentHashMap<>();

    private Map<String, Object> handlerMap = new ConcurrentHashMap<>();

    @Autowired
    RedisConnection redisConnection;

    @Autowired
    AmqQueueSender sender;

    @Autowired
    BaseControlller baseControlller;

    @Autowired
    ImClientSource clientSource;

    /**
     * 连接
     *
     * @param session
     * @throws Exception
     */
    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        String sessionId = session.getId();
        log.info("connect======>sessionId=" + sessionId + ",remote=" + session.getRemoteAddress());
        GameWebSocketSession webSocketSession = new GameWebSocketSession();
        webSocketSession.webSocketSession = session;
        webSocketSession.serverAddress = NetAddressUtil.getRealIp() + ":" + NetAddressUtil.getHttpPort("HTTP/1.1", "http");
        clients.put(sessionId, webSocketSession);
        String result = "{'stream':{'channelId':'" + sessionId + "'}}";
        session.sendMessage(new TextMessage(result));
    }

    /**
     * 接收消息
     *
     * @param session
     * @param message
     * @throws Exception
     */
    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        String msg = message.getPayload();
        log.info(session.getId() + ",receive======>{}", msg);
        try {
            JSONObject obj = JSONObject.parseObject(msg);
            msgRoute(obj, session);
        } catch (Exception e) {
            log.error("could not parse to json \n" + msg, e);
            try {
                String sub = msg.substring(0, msg.indexOf(",")) + "}";
                JSONObject obj = JSONObject.parseObject(sub);
                String id = obj.getString("id");
                session.sendMessage(new TextMessage("{'id' : '" + id + "', 'value' : {'error' : 'could not parse to json' }}"));
            } catch (Exception e1) {
                log.error("msg ==> {} skip", msg);
            }
        }
    }

    /**
     * 报错
     *
     * @param session
     * @param exception
     * @throws Exception
     */
    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) throws Exception {
        GameWebSocketSession gameWebSocketSession = clients.get(session.getId());
        if (gameWebSocketSession.username != null && !gameWebSocketSession.username.equals("")) {
            log.error("获取到连接异常 " + gameWebSocketSession.username + " 断开连接", exception);
        } else {
            log.error("获取到连接异常 内部服务器连接", exception);
        }

    }

    /**
     * 关闭
     *
     * @param session
     * @param status
     * @throws Exception
     */
    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        String sessionId = session.getId();
        String userId = clients.get(sessionId).username;
        log.info("disconnect======>sessionId=" + sessionId + ",remote=" + session.getRemoteAddress() + ",userId=" + userId);
        if (userId != null) {
            try {
                String msg = createJmsMessage(userId, "disconnectClear", sessionId);
                sender.send("room.disconnect.clear", msg);
            } catch (Exception e) {
                log.error(userId + " 用户无法下线 ", e);
            }
        }
        clients.remove(sessionId);
    }

    /**
     * 生成mq的消息
     */
    private String createJmsMessage(String userId, String type, String sessionId) {
        Map<String, Object> map = new HashMap<>();
        map.put("type", type);
        map.put("userId", userId);
        map.put("channelId", sessionId);
        return JSONObject.toJSONString(map).toString();
    }

    /**
     * 路由算法
     *
     * @param obj
     * @param session
     * @throws ClassNotFoundException
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    private void msgRoute(JSONObject obj, WebSocketSession session) {
        String id = obj.getString("id");
        JSONObject value = obj.getJSONObject("value");
        String protocol = value.getString("protocol");
        ShardedJedis jedis = null;
        try {
            jedis = redisConnection.getSingletonInstance();

            switch (ProtocolEnum.find(protocol)) {
                case iq: {
                    String controller = value.getString("controller");
                    String method = value.getString("method");

                    ResponseSet result = new ResponseSet();
                    Map<String, Object> params;
                    if (value.getJSONObject("params") != null) {
                        params = (Map<String, Object>) JSON.parse(value.getJSONObject("params").toString());
                    } else {
                        params = new HashMap<>();
                    }
                    //这里用枚举存放类路径
                    String sessionId = session.getId();

                    if ((controller.equals("userController") && method.equals("login"))
                            || controller.equals("pingController")) {
                        result = invoke(params, controller, method, sessionId);
                    } else {
                        boolean islogin = baseControlller.sessionValid(sessionId);
                        if (islogin) {
                            result = invoke(params, controller, method, sessionId);
                        } else {
                            ResponseValue responseValue = new ResponseValue();
                            responseValue.set(ProtocolEnum.iq, ResponseEnum.UNLOGIN);
                            result.setValue(responseValue);
                        }
                    }
                    if ("pingController".equals(controller) && "response".equals(method)) {
                        break;//如果是客户端返回ping则不作处理
                    } else if ("pingController".equals(controller) && "request".equals(method)) {
                        result.setId(id);
                    } else {
                        result.getValue().controller = controller;
                        result.getValue().method = method;
                        result.getValue().protocol = ProtocolEnum.iq;
                        result.setId(id);
                    }
                    log.info("response={}",JSONObject.toJSONString(result));
                    session.sendMessage(new TextMessage(JSONObject.toJSONString(result)));
                }
                break;
                case message: {
                    String messageType = value.getString("messageType");
                    String roomId = value.getString("roomId");

                    JSONObject message = obj;
                    ThreadPoolManager pool = ThreadPoolManager.getInstance();
                    String transfer = value.getString("transfer");
                    switch (MessageTypeEnum.indexOf(messageType)) {
                        case groupchat:
                            Set<String> chatSet = jedis.smembers(RoomConst.ROOM_USER + roomId);// 拿到房间内的玩家列表
                            final Set<String> user_channels = new HashSet<>();
                            String exceptUser = value.getString("exceptUsers");
                            List<String> exceptUserList = new ArrayList<>();
                            if (exceptUser != null && exceptUser.length() > 0)
                            {
                                exceptUserList = Arrays.asList(exceptUser.split(","));
                            }
                            for (String userId : chatSet) {
                                //通过userId拿到对应的sessionId
                                String sessionId = jedis.get(UserConst.USERID_SESSION_PREFIX + userId);
                                if (sessionId != null && clients.containsKey(sessionId) && !exceptUserList.contains(userId)) {
                                    user_channels.add(sessionId);
                                }
                            }

                            String chatType = value.getString("chatType");
                            //如果该消息为用户聊天消息，则不发给自己
                            if (chatType.equals("1")) {
                                user_channels.remove(session.getId());
                            }
                            int gameId = value.getInteger("gameId");
                            switch (gameId) {
                                case 1:
                                case 4:
                                    String userId = clients.get(session.getId()).username;
                                    Set<String> gamingUsers = jedis.zrange(RoomConst.ROOM_USER_GAMING + roomId, 0, -1);

                                    if(chatType.equals("1")&& value.getJSONObject("message").getInteger("type") == 1
                                            &&gamingUsers.contains(userId)) {
                                        if(gameId == 1){
                                            sender.send("guessPic.groupchat.content", JSONObject.toJSONString(message));
                                        }
                                        else if(gameId==4){
                                            sender.send("guessSong.groupchat.content",JSONObject.toJSONString(message));
                                        }
                                    } else {
                                        pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), user_channels));
                                        if("true".equals(transfer)){
                                            value.remove("transfer");
                                            pool.execute(() -> ClientUtil.sendMsg
                                                    (clientSource.getHost(), clientSource.getPort(), obj.toJSONString()));
                                        }
                                    }
                                    break;
                                default:
                                    pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), user_channels));
                                    if("true".equals(transfer)){
                                        value.remove("transfer");
                                        pool.execute(() -> ClientUtil.sendMsg
                                                (clientSource.getHost(), clientSource.getPort(), obj.toJSONString()));
                                    }
                            }

                            break;
                        case normal://系统下发
                            Set<String> normalSet = jedis.smembers(RoomConst.ROOM_USER + roomId);// 拿到房间内的玩家列表
                            final Set<String> dispatch_channel = new HashSet<>();
                            String exceptUsersStr = value.getString("exceptUsers");
                            List<String> exceptUsers = new ArrayList<>();
                            if (exceptUsersStr != null && exceptUsersStr.length() > 0) {
                                exceptUsers = Arrays.asList(exceptUsersStr.split(","));
                            }
                            for (String userId : normalSet) {
                                //通过userId拿到对应的sessionId
                                String sessionId = jedis.get(UserConst.USERID_SESSION_PREFIX + userId);
                                if (sessionId != null && clients.containsKey(sessionId) && !exceptUsers.contains(userId)) {
                                    dispatch_channel.add(sessionId);
                                }
                            }
                            if (dispatch_channel.contains(session.getId())) {
                                dispatch_channel.remove(session.getId());
                            }

                            message = value.getJSONObject("message");
                            pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), dispatch_channel));

                            if("true".equals(transfer)){
                                value.remove("transfer");
                                pool.execute(() -> ClientUtil.sendMsg
                                        (clientSource.getHost(), clientSource.getPort(), obj.toJSONString()));
                            }

                           /* //将作画的信息发送到mq
                            if( "makePic".equals(message.getJSONObject("value").getString("method"))){
                                log.info("send picCache to mq");
                                sender.send("guessPic.normal.makePic", JSONObject.toJSONString(obj));
                            }
*/
                            break;
                        case headline://下发给某个人
                            String destination = value.getString("destination");
                            final Set<String> headline_channel = new HashSet<>();
                            final List<String> exceptUsers2 = new ArrayList<>();
                            if ("0".equals(destination)) {
                                String exceptUsersStr2 = value.getString("exceptUsers");
                                if (exceptUsersStr2 != null && exceptUsersStr2.length() > 0) {
                                    exceptUsers2.addAll(Arrays.asList(exceptUsersStr2.split(",")));
                                }
                                headline_channel.addAll(clients.keySet().stream().filter(sessionIdTmp ->
                                        !exceptUsers2.contains(sessionIdTmp) && clients.get(sessionIdTmp).username != null).collect(Collectors.toList()));
                            } else {
                                String sessionId = jedis.get(UserConst.USERID_SESSION_PREFIX + destination);
                                if (sessionId != null && clients.containsKey(sessionId)) {
                                    headline_channel.add(sessionId);
                                }
                            }
                            message = value.getJSONObject("message");
                            pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), headline_channel));
                            break;
                        default:
                            break;
                    }
                }
                break;
                case error:
                    break;
                default://stream
                    String operateSessionId = value.getString("sessionId");
                    String operation = value.getString("operation");
                    switch (OperationEnum.indexOf(operation)) {
                        case open:
                            break;
                        default://close
                            log.info("通过重新登录挤下线 断开 {}", operateSessionId);
                            GameWebSocketSession last = clients.get(operateSessionId);
                            if(last != null && last.username != null){
                                last.username = null;
                                clients.put(operateSessionId, last);
                                last.webSocketSession.close();
                            }
                    }
                    break;
            }

        } catch (Throwable exception) {
            log.error("", exception);
        } finally {
            jedis.close();
        }
    }

    /**
     * 调用
     *
     * @param params
     * @param controller
     * @param method
     * @param sessionId
     * @return
     */
    private ResponseSet invoke(Map<String, Object> params, String controller, String method, String sessionId) {
        ResponseSet result = new ResponseSet();
        try {
            Class c = Class.forName(ControllerEnum.find(controller).websocketPath);
            Method met = c.getMethod(method, Map.class);
            Constructor constructor = c.getDeclaredConstructor(ApplicationContext.class);
            //这里每次都new，是否有问题，需不需要放到map里
            //JSONObject data = (JSONObject) met.invoke(constructor.newInstance(appContext), params);
            if (handlerMap.get(controller) == null) {
                handlerMap.put(controller, constructor.newInstance(SpringContextUtil.getApplicationContext()));
            }
            params.put("channelId", sessionId);

            result = (ResponseSet) met.invoke(handlerMap.get(controller), params);
        } catch (Exception e) {
            log.error("invoke failed|controller={}|method={}|channelId={}|params={}", controller, method, sessionId,
                    params, e);
            ResponseValue value = new ResponseValue<>();
            value.set(ProtocolEnum.iq, ResponseEnum.NO_SUCH_CONTROLLER_METHOD);
            result.setValue(value);
        }
        return result;
    }

    /**
     * 下发消息
     *
     * @param sessionId
     * @param msg
     */
    public static void sendToSession(String sessionId, String msg) {
        GameWebSocketSession session = clients.get(sessionId);
        if (session != null) {
            try {
                session.webSocketSession.sendMessage(new TextMessage(msg));
            } catch (IOException e) {
                log.error("下发不到 " + sessionId, e);
            }
        }
    }

}
