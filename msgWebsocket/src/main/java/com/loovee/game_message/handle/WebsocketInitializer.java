package com.loovee.game_message.handle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.server.support.HttpSessionHandshakeInterceptor;

import java.util.List;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/12
 */
@Component
public class WebsocketInitializer extends WebMvcConfigurerAdapter implements WebSocketConfigurer {

    @Autowired
    GameWebsocketHandler gameHander;

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry webSocketHandlerRegistry) {
        webSocketHandlerRegistry.addHandler(gameHander, "/gameWebsocket").
                addInterceptors(new HttpSessionHandshakeInterceptor()).setAllowedOrigins("*");

        webSocketHandlerRegistry.addHandler(gameHander, "/gamesocketjs").withSockJS();
    }

//    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
//
//    }
}
