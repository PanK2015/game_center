package com.loovee.game_message.controller;

import com.loovee.game.message.bean.GameWebSocketSession;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game.truth.service.TruthService;
import com.loovee.game_message.handle.GameWebsocketHandler;
import com.xxx.rpc.client.RpcProxy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.Map;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/26
 */
public class TruthController {

    Logger log = LoggerFactory.getLogger(TruthController.class);

    private ApplicationContext context;

    private RpcProxy rpcProxy;

    public TruthController(ApplicationContext context) {
        this.context = context;
        this.rpcProxy = context.getBean(RpcProxy.class);
    }

    public Object truth(Map<String, Object> params) {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try {
            String roomId = (String) params.get("roomId");
            if (StringUtils.isBlank(roomId)) {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
                responseSet.setValue(responseValue);
                return responseSet;
            }
            String sessionId = (String) params.get("channelId");
            String userId = getUsername(sessionId);
            TruthService truthService = rpcProxy.create(TruthService.class);
            Map<String, Object> result = truthService.truth(userId, roomId);
            int code = (int) result.get("code");
            responseValue.set(ProtocolEnum.iq, ResponseEnum.find(code));
            responseSet.setValue(responseValue);
            return responseSet;
        } catch (Exception e) {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }

    /**
     * 查用户名
     * @param sessionId
     * @return
     */
    public String getUsername(String sessionId){
        GameWebSocketSession session = GameWebsocketHandler.clients.get(sessionId);
        return session.username;
    }
}
