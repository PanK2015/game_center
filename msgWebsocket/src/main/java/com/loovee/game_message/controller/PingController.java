package com.loovee.game_message.controller;

import com.loovee.game.consts.SystemConst;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.iq.ControllerEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.redis.connection.RedisConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import redis.clients.jedis.ShardedJedis;

import java.util.Map;

/**
 * Created by china on 2016/9/27.
 */
public class PingController {

    Logger log = LoggerFactory.getLogger(PingController.class);

    private ApplicationContext context;

    private RedisConnection redisConnection;

    public PingController (ApplicationContext context) {
        this.context = context;
        this.redisConnection = context.getBean(RedisConnection.class);
    }

    /**
     * 接收到response,把数据从buffered和no_received中删除
     * @param params
     * @return
     */
    public Object response(Map<String, Object> params){
        ShardedJedis jedis = redisConnection.getSingletonInstance();
        try{
            String channelId = (String) params.get("channelId");
            String bufferedKey = SystemConst.SENDED_PING_CHANNEL_BUFFERED;
            String noReceivedKey = SystemConst.SENDED_NO_RECEIVED_CHANNEL;
            jedis.srem(bufferedKey, channelId);
            jedis.srem(noReceivedKey, channelId);
        } catch (Exception e) {
            log.error("", e);
        } finally {
            jedis.close();
            return new ResponseSet();
        }
    }

    /**
     * 纯返回
     * @param params
     * @return
     */
    public Object request(Map<String, Object> params){
        ResponseSet set = new ResponseSet();
        ResponseValue value = new ResponseValue();
        value.protocol = ProtocolEnum.iq;
        value.controller = ControllerEnum.pingController.controller;
        value.method = "response";
        set.setValue(value);
        return set;
    }
}
