package com.loovee.game_message.controller;

import com.loovee.game.message.bean.GameWebSocketSession;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game.room.pos.RepoRoom;
import com.loovee.game.room.pos.TempRoom;
import com.loovee.game.room.service.RoomService;
import com.loovee.game.user.bean.GameUser;
import com.loovee.game_message.handle.GameWebsocketHandler;
import com.xxx.rpc.client.RpcProxy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by china on 2016/9/20.
 */
public class RoomController {

    Logger log = LoggerFactory.getLogger(RoomController.class);

    private ApplicationContext context;

    private RpcProxy rpcProxy;

    public RoomController (ApplicationContext context) {
        this.context = context;
        this.rpcProxy = context.getBean(RpcProxy.class);
    }

    /**
     * 快速开始
     *
     * @param params
     * @return
     */
    public Object matchingTempRoom(Map<String, Object> params) {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        TempRoom room = null;
        try
        {
            String gameId = params.get("gameId").toString();
            String userId = getUsername((String) params.get("channelId"));
            RoomService roomService = rpcProxy.create(RoomService.class);
            String roomid = roomService.matchingTempRoom(gameId,userId);
            if (roomid == null)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.NO_SUCH_GAME);
            }
            else if (roomid.equals("exception"))
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            }
            else
            {
                room = roomService.queryTempRoom(roomid);

                Map<String, Object> map = new HashMap<>();
                map.put("room", room);
                responseValue.data = map;
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 搜索房间
     *
     * @param params
     * @return
     */
    public Object search(Map<String, Object> params) {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try
        {
            String roomid = params.get("keyword").toString();
            RoomService roomService = rpcProxy.create(RoomService.class);
            RepoRoom room = roomService.searchRoom(roomid);
            if (room == null)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.NO_ROOM);
            }
            else
            {
                Map<String, Object> map = new HashMap<>();
                map.put("room", room);
                responseValue.data = map;
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }

    /**
     * 我的房间
     *
     * @param params
     * @return
     */
    public Object myRoom(Map<String, Object> params) {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try
        {
            String userid = (String) params.get("userId");
            RoomService roomService = rpcProxy.create(RoomService.class);
            List<RepoRoom> myrooms = roomService.myRoom(userid);
            if (myrooms == null)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.NO_SUCH_USER);
            }
            else
            {
                Map<String, Object> map = new HashMap<>();
                map.put("room", myrooms);
                responseValue.data = map;
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 进入房间
     *
     * @param params
     * @return
     */
    public Object enterRoom(Map<String, Object> params) {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue<Map<String, Object>> responseValue = new ResponseValue<>();
        try
        {
            String roomId = (String) params.get("roomId");
            if (StringUtils.isBlank(roomId))
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
            }
            else
            {
                RoomService roomService = rpcProxy.create(RoomService.class);
                String username = getUsername((String) params.get("channelId"));
                Map<String, Object> map = roomService.enterRoom(roomId, username);

                if (map == null)
                {
                    responseValue.set(ProtocolEnum.iq, ResponseEnum.NO_ROOM);
                }
                else
                {
                    if (map.get("error") != null)
                    {
                        responseValue.set(ProtocolEnum.iq, ResponseEnum.find((Integer) map.get("error")));
                    }
                    else
                    {
                        responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS, map);
                    }
                }
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }

    /**
     * 退出房间
     *
     * @param params
     * @return
     */
    public Object outRoom(Map<String, Object> params) {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try
        {
            RoomService roomService = rpcProxy.create(RoomService.class);
            String roomId = (String) params.get("roomId");
            log.info("outRoom ==> roomId : {}", roomId);
            if (StringUtils.isBlank(roomId))
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
            }
            else
            {
                String username = getUsername((String) params.get("channelId"));
                int code = roomService.outRoom(roomId, username);
                responseValue.set(ProtocolEnum.iq, ResponseEnum.find(code));
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 查用户名
     * @param sessionId
     * @return
     */
    public String getUsername(String sessionId){
        GameWebSocketSession session = GameWebsocketHandler.clients.get(sessionId);
        return session.username;
    }

    /**
     * 获取房间内用户资料
     *
     * @param params
     * @return
     */
    public Object userInfo(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try
        {
            String userId = params.get("userId").toString();

            log.info("userInfo params|userId={}", userId);

            RoomService roomService = rpcProxy.create(RoomService.class);
            GameUser user = roomService.userInfo(userId);
            if (user != null)
            {
                if(user.getAge()!= null)
                {
                    String birthday = user.getAge().split("-")[0];
                    String currentYear = new SimpleDateFormat("yyyy").format(new Date());
                    int age = Integer.parseInt(currentYear) - Integer.parseInt(birthday);
                    user.setAge(String.valueOf(age));
                }
                Map<String,Object> map = new HashMap<>();
                map.put("user",user);
                responseValue.data = map;
                responseValue.set(ProtocolEnum.iq,ResponseEnum.SUCCESS);
            }
            else
            {
                responseValue.set(ProtocolEnum.iq,ResponseEnum.NO_SUCH_USER);
            }

        }
        catch (Exception e)
        {
            log.error("",e);
            responseValue.set(ProtocolEnum.iq,ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }
}
