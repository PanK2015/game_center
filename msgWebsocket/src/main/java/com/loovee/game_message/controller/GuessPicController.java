package com.loovee.game_message.controller;

import com.loovee.game.consts.RoomConst;
import com.loovee.game.guessPic.service.GuessPicService;
import com.loovee.game.message.bean.GameWebSocketSession;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game.room.config.RoomStatus;
import com.loovee.game_message.handle.GameWebsocketHandler;
import com.loovee.redis.connection.RedisConnection;
import com.xxx.rpc.client.RpcProxy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import redis.clients.jedis.ShardedJedis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by china on 2016/9/22.
 */
public class GuessPicController
{

    Logger log = LoggerFactory.getLogger(GuessPicController.class);

    private ApplicationContext context;

    private RpcProxy rpcProxy;

    private RedisConnection redisConnection;

    public GuessPicController(ApplicationContext context)
    {
        this.context = context;
        this.rpcProxy = context.getBean(RpcProxy.class);
        this.redisConnection = context.getBean(RedisConnection.class);
    }

    public Object ready(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try
        {
            String roomId = (String) params.get("roomId");
            String userId = (String) params.get("userId");
            GuessPicService guessPicService = rpcProxy.create(GuessPicService.class);
            int ret = guessPicService.ready(roomId, userId);
            if (ret == -2)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.NO_SUCH_USER);
            }
            else if (ret == -1)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            }
            else
            {
                Map<String, Object> map = new HashMap<>();
                map.put("userNum", ret);
                responseValue.data = map;
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }

        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 换一批
     *
     * @param params
     * @return
     */
    public Object changeWord(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();

        List<Integer> currentStrs = new ArrayList<Integer>(4);
        try
        {
            currentStrs.add(Integer.parseInt((String) params.get("word1")));
            currentStrs.add(Integer.parseInt((String) params.get("word2")));
            currentStrs.add(Integer.parseInt((String) params.get("word3")));
            currentStrs.add(Integer.parseInt((String) params.get("word4")));
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
            responseSet.setValue(responseValue);
            return responseSet;
        }
        try
        {
            GuessPicService guessPicService = rpcProxy.create(GuessPicService.class);
            String userId = getUsername((String) params.get("channelId"));
            Map<String, Object> result = guessPicService.changeWord(userId, currentStrs);
            int code = (int) result.get("code");
            if (code == 200)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
                responseValue.data = result.get("words");
            }
            else
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.find(code));
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 查用户名
     *
     * @param sessionId
     * @return
     */
    public String getUsername(String sessionId)
    {
        GameWebSocketSession session = GameWebsocketHandler.clients.get(sessionId);
        return session.username;
    }

    /**
     * 选词
     *
     * @param params
     * @return
     */
    public Object chooseWord(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try {
            String roomId = (String) params.get("roomId");
            String word = (String) params.get("wordId");

            if (StringUtils.isBlank(roomId) || StringUtils.isBlank(word)) {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
                responseSet.setValue(responseValue);
                return responseSet;
            }
            GuessPicService guessPicService = rpcProxy.create(GuessPicService.class);
            Map<String, Object> map = guessPicService.chooseWord(roomId, word);

            responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS, map);
        } catch (Exception e) {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        } finally {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }
}
