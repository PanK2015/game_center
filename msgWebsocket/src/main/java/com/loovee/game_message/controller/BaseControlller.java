package com.loovee.game_message.controller;

import com.loovee.game.message.bean.GameWebSocketSession;
import com.loovee.game_message.handle.GameWebsocketHandler;
import org.springframework.stereotype.Controller;

/**
 * Created by loovee on 2016/9/21.
 */
@Controller(value = "baseController")
public class BaseControlller {

    public boolean sessionValid(String sessionId){
        GameWebSocketSession session = GameWebsocketHandler.clients.get(sessionId);
        if(session.username != null){
            return true;
        }
        return false;
    }
}
