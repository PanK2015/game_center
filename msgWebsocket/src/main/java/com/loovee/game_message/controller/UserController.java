package com.loovee.game_message.controller;

import com.loovee.game.config.pos.GameConfig;
import com.loovee.game.message.bean.GameWebSocketSession;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game.user.requests.LoginUserRequestParams;
import com.loovee.game.user.service.UserService;
import com.loovee.game.utils.DESCBC;
import com.loovee.game_message.handle.GameWebsocketHandler;
import com.loovee.game_message.util.JsonUtil;
import com.loovee.redis.connection.RedisConnection;
import com.xxx.rpc.client.RpcProxy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by china on 2016/9/20.
 */
public class UserController {

    Logger log = LoggerFactory.getLogger(UserController.class);

    private ApplicationContext context;

    private RpcProxy rpcProxy;

    private RedisConnection redisConnection;

    public UserController (ApplicationContext context) {
        this.context = context;
        this.rpcProxy = context.getBean(RpcProxy.class);
        this.redisConnection = context.getBean(RedisConnection.class);
    }

    /**
     * 登录
     *
     * @param params
     * @return
     */

    public Object login(Map<String, Object> params)
    {

        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();

        LoginUserRequestParams param = JsonUtil.MapToBean(params, LoginUserRequestParams.class);
        String key = "s@x$cp.j";

        try
        {

            param.setAppUserId(DESCBC.decryptDES(param.getAppUserId(),key));
            param.setAppPassword(DESCBC.decryptDES(param.getAppPassword(),key));

            if (StringUtils.isBlank(param.getAppName()))
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID, "appname is null");
                responseSet.setValue(responseValue);
                return responseSet;
            }
            if (StringUtils.isBlank(param.getAppUserId()))
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID, "userid is null");

                responseSet.setValue(responseValue);
                return responseSet;
            }
            if (StringUtils.isBlank(param.getAppPassword()))
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID, "password is null");
                responseSet.setValue(responseValue);
                return responseSet;
            }
            if(StringUtils.isBlank(param.getClientType()))
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID, "clientType is null");
                responseSet.setValue(responseValue);
                return responseSet;
            }

            UserService userService = rpcProxy.create(UserService.class);
            Map<String, Object> loginInfo = userService.login(param);

            if (loginInfo.get("code").equals(200))
            {//登陆成功
                GameWebSocketSession session = GameWebsocketHandler.clients.get(param.getChannelId());
                session.username = param.getAppUserId();

                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
                Map map = new HashMap();
                map.put("userid", param.getAppUserId());
                responseValue.data = map;
                log.info("login sucess");
            }
            else
            {//登录失败
                log.info("login failed");
                responseValue.set(ProtocolEnum.iq, ResponseEnum.find((Integer) loginInfo.get("code")));
            }

        }
        catch (Exception e)
        {
            log.error("", e);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }
    /**
     * 查用户名
     * @param sessionId
     * @return
     */
    public String getUsername(String sessionId){
        GameWebSocketSession session = GameWebsocketHandler.clients.get(sessionId);
        return session.username;
    }

    /**
     * 首页
     * @param params
     * @return
     */
    public Object homepage(Map<String, Object> params)
    {

        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try
        {
            UserService userService = rpcProxy.create(UserService.class);
            String username = this.getUsername((String) params.get("channelId"));
            List<GameConfig> list = userService.homepage(username);
            Map<String, Object> map = new HashMap<>();
            map.put("game", list);
            responseValue.data = map;
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);

        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 邀请好友
     *
     * @param params
     * @return
     */
    public Object inviteFriend(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();

        try
        {
            String userId = params.get("userId").toString();
            int start = (int) params.get("start");
            int end = (int) params.get("end");
            UserService userService = rpcProxy.create(UserService.class);
            Map<String, Object> users = userService.inviteFriend(userId, start, end);
            if (users != null)
            {

                Map<String, Object> map = new HashMap<>();
                map.put("friend", users);
                responseValue.data = map;
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }
            else
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 发送邀请好友的消息
     *
     * @param params
     * @return
     */
    public Object inviteMsg(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();

        try
        {
            String from_user = params.get("from_user").toString();
            String recv_user = params.get("recv_user").toString();
            String roomId = params.get("roomId").toString();
            String gameName = params.get("game_name").toString();

            UserService userService = rpcProxy.create(UserService.class);
            int code = userService.inviteMsg(from_user, recv_user, roomId, gameName);
            if (code == 200)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }
            else
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            }

        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 送礼通知
     * @param params
     * @return
     */
    public Object giftNotice(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();

        try
        {
            String giftId = params.get("giftId").toString();
            String roomId = params.get("roomId").toString();
            String userId = params.get("userId").toString();

            UserService userService = rpcProxy.create(UserService.class);
            int code = userService.giftNotice(giftId,roomId,userId);

            if (code == 200)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }
            else
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }
}


