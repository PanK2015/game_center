package com.loovee.game_message.controller;

import com.loovee.game.consts.RoomConst;
import com.loovee.game.guessSong.service.GuessSongService;
import com.loovee.game.message.bean.GameWebSocketSession;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game.room.config.RoomStatus;
import com.loovee.game_message.handle.GameWebsocketHandler;
import com.loovee.redis.connection.RedisConnection;
import com.xxx.rpc.client.RpcProxy;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import redis.clients.jedis.ShardedJedis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by loovee on 2016/10/26.
 */
public class GuessSongController extends BaseControlller
{
    Logger log = LoggerFactory.getLogger(GuessSongController.class);

    private ApplicationContext context;

    private RpcProxy rpcProxy;

    private RedisConnection redisConnection;

    public GuessSongController(ApplicationContext app)
    {
        this.context = context;
        this.rpcProxy = context.getBean(RpcProxy.class);
        this.redisConnection = context.getBean(RedisConnection.class);
    }

    /**
     * 准备
     */
    public Object ready(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try
        {
            String roomId = (String) params.get("roomId");
           // String userId = (String) params.get("userId");
            String userId = getUsername((String) params.get("channelId"));
            GuessSongService guessSongService = rpcProxy.create(GuessSongService.class);
            int ret = guessSongService.ready(roomId, userId);
            if (ret == -2)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.NO_SUCH_USER);
            }
            else if (ret == -1)
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            }
            else
            {
                Map<String, Object> map = new HashMap<>();
                map.put("userNum", ret);
                responseValue.data = map;
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }

        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 换一批
     *
     * @param params
     * @return
     */
    public Object changeSong(Map<String, Object> params)
    {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();

        List<Integer> currentStrs = new ArrayList<Integer>(1);
        ShardedJedis jedis = null;
        String roomId = params.get("roomId").toString();
        String songId = params.get("songId").toString();
        try
        {
            currentStrs.add(Integer.parseInt(songId));

        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
            responseSet.setValue(responseValue);
            return responseSet;
        }
        try
        {
            GuessSongService guessSongService = rpcProxy.create(GuessSongService.class);
            String userId = getUsername((String) params.get("channelId"));
            Map<String, Object> result = guessSongService.changeSong(userId, currentStrs);
            int code = (int) result.get("code");
            if (code == 200)
            {
                jedis = context.getBean(RedisConnection.class).getSingletonInstance();
                if (!String.valueOf(RoomStatus.GUESS_SONG_STATUS_GAMING)
                    .equals(jedis.hget(RoomConst.ROOM_INFO + roomId, "status")))
                {
                    String songid =((Map)result.get("song")).get("songId").toString();
                    jedis.hset(RoomConst.GUESS_SONG_ROOM_TIME+roomId,"song",songid);
                }

                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);

                Map<String, Object> map = new HashMap<>();
                map.put("songInfo", result.get("song"));
                responseValue.data = map;
            }
            else
            {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.find(code));
            }
        }
        catch (Exception e)
        {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
        }
        finally
        {
            jedis.close();
            responseSet.setValue(responseValue);
            return responseSet;
        }

    }

    /**
     * 查用户名
     *
     * @param sessionId
     * @return
     */
    public String getUsername(String sessionId)
    {
        GameWebSocketSession session = GameWebsocketHandler.clients.get(sessionId);
        return session.username;
    }

}
