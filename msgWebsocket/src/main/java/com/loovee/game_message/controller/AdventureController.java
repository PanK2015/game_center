package com.loovee.game_message.controller;

import com.loovee.game.adventure.service.AdventureService;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.xxx.rpc.client.RpcProxy;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.Map;

/**
 * Created by china on 2016/9/20.
 */
public class AdventureController {

    Logger logger = LoggerFactory.getLogger(AdventureController.class);

    private ApplicationContext context;

    private RpcProxy rpcProxy;

    public AdventureController (ApplicationContext context) {
        this.context = context;
        this.rpcProxy = context.getBean(RpcProxy.class);
    }

    /**
     * 准备
     * @param params
     * @return
     */
    public Object ready(Map<String,Object> params){
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try {
            String roomId = (String) params.get("roomId");
            String userId = (String) params.get("userId");
            if(StringUtils.isBlank(roomId) || StringUtils.isBlank(userId)){
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
            }
            AdventureService adventureService = rpcProxy.create(AdventureService.class);
            int ret = adventureService.ready(roomId, userId);
            if (ret == -2) {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.NO_SUCH_USER);
            } else if (ret == -1) {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            } else {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.SUCCESS);
            }
            responseSet.setValue(responseValue);
            return responseSet;
        } catch (Exception e) {
            logger.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }

    /**
     * 冒险
     * @param params
     * @return
     */
    public Object adventure(Map<String,Object> params){
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try {
            String roomId = (String) params.get("roomId");
            String userId = (String) params.get("userId");
            int random = (int) params.get("random");
            if(StringUtils.isBlank(roomId) || StringUtils.isBlank(userId) || random == 0){
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
            }
            AdventureService adventureService = rpcProxy.create(AdventureService.class);
            Map<String, Object> map = adventureService.adventure(roomId, userId, random);
            int code = (Integer) map.get("code");

            responseValue.set(ProtocolEnum.iq, ResponseEnum.find(code));
            responseSet.setValue(responseValue);
            return responseSet;
        } catch (Exception e) {
            logger.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }

}
