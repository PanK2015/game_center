package com.loovee.game;

import com.loovee.game.handler.GameWebSocketClientHandler;

import javax.websocket.CloseReason;
import javax.websocket.ContainerProvider;
import javax.websocket.Session;
import javax.websocket.WebSocketContainer;
import java.io.IOException;
import java.net.URI;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/14
 */
public class GameWebSocketClient {

    WebSocketContainer container = ContainerProvider.getWebSocketContainer();

    Session session;

    private String host;

    private int port;

    private String field;

    public GameWebSocketClient(String host, int port, String field) {
        container.setDefaultMaxBinaryMessageBufferSize(8192);
        container.setDefaultMaxTextMessageBufferSize(8192);
        container.setAsyncSendTimeout(180000);
        container.setDefaultMaxSessionIdleTimeout(300000);
        this.host = host;
        this.port = port;
        this.field = field;
    }

    public void connect() throws Exception {
        String uriStr = "ws://" + host + ":" + port + field;
        URI uri = new URI(uriStr);
        session = container.connectToServer(GameWebSocketClientHandler.class, uri);
    }

    public void sendMsg(String msg) {
        try {
            connect();
            session.getBasicRemote().sendText(msg, true);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            try {
                if(session != null){
                    session.close(new CloseReason(CloseReason.CloseCodes.NORMAL_CLOSURE, "发送完毕"));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
