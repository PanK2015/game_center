package com.loovee.game.handler;

import javax.websocket.*;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/13
 */
@ClientEndpoint
public class GameWebSocketClientHandler {

    @OnOpen
    public void onOpen(Session session) {
        System.out.println("connect to endpoint: " + session.getBasicRemote());
    }

    @OnMessage
    public void onMessage(String message) {
        System.out.println(message);
    }

    @OnError
    public void onError(Throwable t) {
        t.printStackTrace();
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println("disconnect to endpoint: " + session.getBasicRemote());
    }

}
