package com.loovee.msgserver.controller;

import com.loovee.game.message.bean.ChannelSession;
import com.loovee.msgserver.handle.SecureMsgServerHandler;
import com.xxx.rpc.client.RpcProxy;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

/**
 * Created by loovee on 2016/9/21.
 */
@Component
public class BaseControlller //implements ApplicationContextAware
{
    public ApplicationContext appContext;
    RpcProxy rpcProxy;

    public BaseControlller()
    {

    }

    public BaseControlller(ApplicationContext appContext)
    {
        this.appContext = appContext;
        this.rpcProxy = appContext.getBean(RpcProxy.class);
    }

    public boolean sessionValid(String channelId){
        ChannelSession session = SecureMsgServerHandler.clients.get(channelId);
        if(session.username != null){
            return true;
        }
        return false;
    }

    public String getUsername(String channelId){
        ChannelSession session = SecureMsgServerHandler.clients.get(channelId);
        return session.username;
    }

}
