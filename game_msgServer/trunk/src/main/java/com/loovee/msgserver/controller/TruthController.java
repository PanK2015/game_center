package com.loovee.msgserver.controller;

import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game.truth.service.TruthService;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import java.util.Map;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/26
 */
public class TruthController extends BaseControlller {

    Logger log = LoggerFactory.getLogger(TruthController.class);

    public TruthController(ApplicationContext app)
    {
        super(app);
    }

    public Object truth(Map<String, Object> params) {
        ResponseSet responseSet = new ResponseSet();
        ResponseValue responseValue = new ResponseValue();
        try {
            String roomId = (String) params.get("roomId");
            if (StringUtils.isBlank(roomId)) {
                responseValue.set(ProtocolEnum.iq, ResponseEnum.PARAM_INVALID);
                responseSet.setValue(responseValue);
                return responseSet;
            }
            String sessionId = (String) params.get("channelId");
            String userId = getUsername(sessionId);
            TruthService truthService = rpcProxy.create(TruthService.class);
            Map<String, Object> result = truthService.truth(userId, roomId);
            int code = (int) result.get("code");
            responseValue.set(ProtocolEnum.iq, ResponseEnum.find(code));
            responseSet.setValue(responseValue);
            return responseSet;
        } catch (Exception e) {
            log.error("", e);
            responseValue.set(ProtocolEnum.iq, ResponseEnum.SYSTEM_ERROR);
            responseSet.setValue(responseValue);
            return responseSet;
        }
    }

}
