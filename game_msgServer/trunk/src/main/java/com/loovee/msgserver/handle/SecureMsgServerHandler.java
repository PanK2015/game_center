package com.loovee.msgserver.handle;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.loovee.game.consts.RoomConst;
import com.loovee.game.consts.SystemConst;
import com.loovee.game.consts.UserConst;
import com.loovee.game.message.bean.ChannelSession;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;
import com.loovee.game.protocol.enums.iq.ControllerEnum;
import com.loovee.game.protocol.enums.message.MessageTypeEnum;
import com.loovee.game.protocol.enums.stream.OperationEnum;
import com.loovee.game.protocol.params.RequestSet;
import com.loovee.game.protocol.params.RequestValue;
import com.loovee.game.protocol.params.ResponseSet;
import com.loovee.game.protocol.params.ResponseValue;
import com.loovee.game.utils.IdUtil;
import com.loovee.msgserver.MsgServerApplication;
import com.loovee.msgserver.config.WebSocketClientSource;
import com.loovee.msgserver.controller.BaseControlller;
import com.loovee.msgserver.jms.AmqQueueSender;
import com.loovee.msgserver.util.ClientUtil;
import com.loovee.msgserver.util.NetAddressUtil;
import com.loovee.msgserver.util.SendMsgToAll;
import com.loovee.msgserver.util.ThreadPoolManager;
import com.loovee.redis.connection.RedisConnection;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.timeout.IdleState;
import io.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import redis.clients.jedis.ShardedJedis;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

/**
 * Created by loovee on 2016/9/19.
 */
public class SecureMsgServerHandler extends SimpleChannelInboundHandler<String>
{

    String currentAddress;

    ConfigurableApplicationContext appContext;

    private Map<String, Object> handlerMap = new HashMap<>();

    public static Map<String, ChannelSession> clients = new ConcurrentHashMap<>();

    Logger log = LoggerFactory.getLogger(SecureMsgServerHandler.class);

    private WebSocketClientSource webSocketClientSource;

    public SecureMsgServerHandler()
    {
        super();
    }

    public SecureMsgServerHandler(ConfigurableApplicationContext appContext)
    {
        try
        {
            currentAddress = NetAddressUtil.getRealIp() + ":" + MsgServerApplication.port;
            this.appContext = appContext;
            webSocketClientSource = appContext.getBean(WebSocketClientSource.class);
        }
        catch (Exception e)
        {
            log.error("", e);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception
    {
        String channelId = ctx.channel().id().asLongText();
        log.info("connect======>channelId=" + channelId + ",remote=" + ctx.channel().remoteAddress());
        ChannelSession channelSession = new ChannelSession();
        channelSession.serverAddress = currentAddress;
        channelSession.channel = ctx.channel();
        clients.put(channelId, channelSession);
        String result = "{'stream':{'channelId':'" + channelId + "'}}";

        ctx.channel().writeAndFlush(result);

        // super.channelActive(ctx);

    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception
    {
        String channelId = ctx.channel().id().asLongText();
        String userId = clients.get(channelId).username;
        log.info("disconnect======>channelId=" + channelId + ",remote=" + ctx.channel().remoteAddress() + ",userId=" + userId);
        if (userId != null)
        {
            try
            {
                AmqQueueSender sender = appContext.getBean(AmqQueueSender.class);
                String msg = createJmsMessage(userId,"disconnectClear", channelId);
                sender.send("room.disconnect.clear",msg);
            }
            catch (Exception e)
            {
                log.error(userId + " 用户无法下线 ", e);
            }
        }
        clients.remove(channelId);
    }


    /**
     * 生成mq的消息
     *
     * @param userId
     * @param type
     * @param channelId
     */
    private String createJmsMessage(String userId, String type, String channelId)
    {
        Map<String, Object> map = new HashMap<>();
        map.put("type", type);
        map.put("userId", userId);
        map.put("channelId", channelId);
        return JSONObject.toJSONString(map).toString();
    }



    /**
     * 收到消息之后的相应响应
     *
     * @param ctx
     * @param msg
     * @throws Exception
     */
    @Override
    protected void messageReceived(ChannelHandlerContext ctx, String msg) throws Exception
    {
        log.info(ctx.channel().id().asLongText() + ",receive======>{}", msg);
        try
        {
            JSONObject obj = JSONObject.parseObject(msg);
            msgRoute(obj, ctx);
        }
        catch (Exception e)
        {
            log.error("could not parse to json \n" + msg, e);
            try
            {
                String sub = msg.substring(0, msg.indexOf(",")) + "}";
                JSONObject obj = JSONObject.parseObject(sub);
                String id = obj.getString("id");
                ctx.writeAndFlush("{'id' : '" + id + "', 'value' : {'error' : 'could not parse to json' }}");
            }
            catch (Exception e1)
            {
                log.error("msg ==> {} skip", msg);
            }

        }

    }

    /**
     * 心跳
     *
     * @param ctx
     * @param evt
     * @throws Exception
     */
    @Override
    public void userEventTriggered(ChannelHandlerContext ctx, Object evt) throws Exception
    {
        super.userEventTriggered(ctx, evt);
        if (evt instanceof IdleStateEvent)
        {
            String channelId = ctx.channel().id().asLongText();
            IdleStateEvent event = (IdleStateEvent) evt;
            if (event.state().equals(IdleState.READER_IDLE))
            {
                //60秒内该条连接无数据上传
                log.debug("channel:{}, user{} with no data upload with 60s", channelId, clients.get(channelId).username);
            }
            else if (event.state().equals(IdleState.WRITER_IDLE))
            {
                //30秒内该条连接无数据下发
                log.debug("channel:{} with no data download with 30s", channelId, clients.get(channelId).username);
            }
            else if (event.state().equals(IdleState.ALL_IDLE))
            {
                //90秒读写(其他)均空闲
                ShardedJedis jedis = appContext.getBean(RedisConnection.class).getSingletonInstance();
                try
                {
                    RequestSet set = new RequestSet();
                    String id = IdUtil.generateId(channelId, jedis);
                    set.setId(id);
                    RequestValue value = new RequestValue();
                    value.setController(ControllerEnum.pingController);
                    value.setProtocol(ProtocolEnum.iq);
                    value.setMethod("request");
                    set.setValue(value);
                    ChannelFuture future = ctx.channel().writeAndFlush(JSONObject.toJSONString(set));
                    String key = SystemConst.SENDED_PING_CHANNEL_BUFFERED;
                    jedis.sadd(key, channelId);
                    String userId = clients.get(channelId).username == null ? "" : clients.get(channelId).username;
                    log.info("send ping ====> channelId={}, userId={}", channelId, userId);
                } catch (Exception e) {
                    log.error("", e);
                }
                finally
                {
                    jedis.close();
                }
            }

        }

    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception
    {
        log.error("error occur =====> {}", cause);
        super.exceptionCaught(ctx, cause);
    }

    /**
     * 消息路由
     *
     * @param obj
     */
    private void msgRoute(JSONObject obj, ChannelHandlerContext ctx)
        throws ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InstantiationException,
        InvocationTargetException
    {
        String id = obj.getString("id");
        JSONObject value = obj.getJSONObject("value");
        String protocol = value.getString("protocol");
        ShardedJedis jedis = null;
        try
        {
            jedis = appContext.getBean(RedisConnection.class).getSingletonInstance();
            switch (ProtocolEnum.find(protocol))
            {
            case iq:
                String controller = value.getString("controller");
                String method = value.getString("method");

                ResponseSet result = new ResponseSet();
                Map<String, Object> params = null;
                if (value.getJSONObject("params") != null)
                {
                    params = (Map<String, Object>) JSON.parse(value.getJSONObject("params").toString());
                }
                else
                {
                    params = new HashMap<>();
                }
                //这里用枚举存放类路径
                String channelId = ctx.channel().id().asLongText();

                if ((controller.equals("userController") && method.equals("login"))
                    || controller.equals("pingController"))
                {
                    result = invoke(params, controller, method, channelId);
                }
                else
                {
                    BaseControlller baseControlller = appContext.getBean(BaseControlller.class);
                    boolean islogin = baseControlller.sessionValid(ctx.channel().id().asLongText());
                    if (islogin)
                    {
                        result = invoke(params, controller, method, channelId);
                    }
                    else
                    {
                        ResponseValue responseValue = new ResponseValue();
                        responseValue.set(ProtocolEnum.iq, ResponseEnum.UNLOGIN);
                        result.setValue(responseValue);
                    }
                }
                if ("pingController".equals(controller) && "response".equals(method))
                {
                    break;//如果是客户端返回ping则不作处理
                }
                else if ("pingController".equals(controller) && "request".equals(method))
                {
                    result.setId(id);
                }
                else
                {
                    result.getValue().controller = controller;
                    result.getValue().method = method;
                    result.getValue().protocol = ProtocolEnum.iq;
                    result.setId(id);
                }

                log.info("response={}",JSONObject.toJSONString(result));
                ctx.writeAndFlush(JSONObject.toJSONString(result));
                break;
            case message:
                String messageType = value.getString("messageType");
                String roomid = value.getString("roomId");

                JSONObject message = obj;
                ThreadPoolManager pool = ThreadPoolManager.getInstance();
                String transfer = value.getString("transfer");
                switch (MessageTypeEnum.indexOf(messageType))
                {
                case groupchat:
                    Set<String> chatSet = jedis.smembers(RoomConst.ROOM_USER + roomid);// 拿到房间内的玩家列表
                    final Set<String> user_channels = new HashSet<>();
                    String exceptUser = value.getString("exceptUsers");
                    List<String> exceptUserList = new ArrayList<>();
                    if (exceptUser != null && exceptUser.length() > 0)
                    {
                        exceptUserList = Arrays.asList(exceptUser.split(","));
                    }
                    for (String userid : chatSet)
                    {
                        //通过userid拿到对应的channelid
                        String channelid = jedis.get(UserConst.USERID_CHANNEL_PREFIX + userid);
                        if (channelid != null && clients.containsKey(channelid) && !exceptUserList.contains(userid))
                        {
                            user_channels.add(channelid);
                        }
                    }


                    String chatType = value.getString("chatType");
                    //如果该消息为用户聊天消息，则不发给自己
                    if (chatType.equals("1"))
                    {
                        user_channels.remove(ctx.channel().id().asLongText());
                    }
                    int gameId = value.getInteger("gameId");
                    switch (gameId)
                    {
                    case 1:
                    case 4:
                        String userId = clients.get(ctx.channel().id().asLongText()).username;
                        Set<String> gamingUsers = jedis.zrange(RoomConst.ROOM_USER_GAMING+roomid,0,-1);

                        if(chatType.equals("1")&& value.getJSONObject("message").getInteger("type") == 1
                            &&gamingUsers.contains(userId)) {
                            log.info("message={}",message);
                            AmqQueueSender sender = appContext.getBean(AmqQueueSender.class);
                            if(gameId == 1){
                                sender.send("guessPic.groupchat.content", JSONObject.toJSONString(message));
                            }
                            else if(gameId==4){
                                sender.send("guessSong.groupchat.content",JSONObject.toJSONString(message));
                            }
                        } else {
                            pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), user_channels));
                            if("true".equals(transfer)){
                                value.remove("transfer");
                                pool.execute(() -> ClientUtil.sendWebSocketMsg(webSocketClientSource.getHost(),
                                        webSocketClientSource.getPort(), webSocketClientSource.getField(), obj.toJSONString()));
                            }
                        }
                        break;
                    default:
                        pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), user_channels));
                        if("true".equals(transfer)){
                            value.remove("transfer");
                            pool.execute(() -> ClientUtil.sendWebSocketMsg(webSocketClientSource.getHost(),
                                    webSocketClientSource.getPort(), webSocketClientSource.getField(), obj.toJSONString()));
                        }
                    }

                    break;
                case normal://系统下发
                    Set<String> normalSet = jedis.smembers(RoomConst.ROOM_USER + roomid);// 拿到房间内的玩家列表
                    final Set<String> dispatch_channel = new HashSet<>();
                    String exceptUsersStr = value.getString("exceptUsers");
                    List<String> exceptUsers = new ArrayList<>();
                    if (exceptUsersStr != null && exceptUsersStr.length() > 0)
                    {
                        exceptUsers = Arrays.asList(exceptUsersStr.split(","));
                    }
                    for (String userid : normalSet)
                    {
                        //通过userid拿到对应的channelid
                        String channelid = jedis.get(UserConst.USERID_CHANNEL_PREFIX + userid);
                        if (channelid != null && clients.containsKey(channelid) && !exceptUsers.contains(userid))
                        {
                            dispatch_channel.add(channelid);
                        }
                    }
                   if(dispatch_channel.contains(ctx.channel().id().asLongText())){
                       dispatch_channel.remove(ctx.channel().id().asLongText());
                   }

                    message = value.getJSONObject("message");
                    pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), dispatch_channel));

                    if("true".equals(transfer)){
                        value.remove("transfer");
                        pool.execute(() -> ClientUtil.sendWebSocketMsg(webSocketClientSource.getHost(),
                                webSocketClientSource.getPort(), webSocketClientSource.getField(), obj.toJSONString()));
                    }

                    //将作画的信息发送到mq
                   /* if( "makePic".equals(message.getJSONObject("value").getString("method"))){
                        log.info("send picCache to mq");
                        AmqQueueSender sender = appContext.getBean(AmqQueueSender.class);
                        sender.send("guessPic.normal.makePic", JSONObject.toJSONString(obj));
                    }*/

                    break;
                    case headline://下发给某个人
                    String destination = value.getString("destination");
                    final Set<String> headline_channel = new HashSet<>();
                    final List<String> exceptUsers2 = new ArrayList<>();
                    if("0".equals(destination)){
                        String exceptUsersStr2 = value.getString("exceptUsers");
                        if (exceptUsersStr2 != null && exceptUsersStr2.length() > 0)
                        {
                            exceptUsers2.addAll(Arrays.asList(exceptUsersStr2.split(",")));
                        }
                        headline_channel.addAll(clients.keySet().stream().filter(channelIdTmp ->
                                !exceptUsers2.contains(channelIdTmp) && clients.get(channelIdTmp).username != null).collect(Collectors.toList()));
                    } else {
                        String channelid = jedis.get(UserConst.USERID_CHANNEL_PREFIX + destination);
                        if (channelid != null && clients.containsKey(channelid))
                        {
                            headline_channel.add(channelid);
                        }
                    }
                    message = value.getJSONObject("message");
                    pool.execute(new SendMsgToAll(JSONObject.toJSONString(message), headline_channel));
                    break;
                default:
                    break;
                }
                break;
            case error:
                break;
            default://stream
                String operateChannelId = value.getString("channelId");
                String operation = value.getString("operation");
                switch (OperationEnum.indexOf(operation)) {
                    case open:
                        break;
                    default://close
                        ChannelSession last = clients.get(operateChannelId);
                        if(last != null && last.username != null){
                            last.username = null;
                            clients.put(operateChannelId, last);
                            last.channel.close();
                        }
                }
                break;
            }
        }
        catch (Exception e)
        {
            log.error("", e);
        }
        finally
        {
            try
            {
                if (jedis != null)
                {
                    jedis.close();
                }
            }
            catch (Exception e)
            {
                log.error("", e);
            }
        }
    }

    public static void sendToChannel(String destChannelId, String msg)
    {
        ChannelSession session = clients.get(destChannelId);
        if (session != null)
        {
            session.channel.writeAndFlush(msg);
        }
    }

    private ResponseSet invoke(Map<String, Object> params, String controller, String method, String channelid)
    {

        ResponseSet result = new ResponseSet();
        try
        {
            Class c = Class.forName(ControllerEnum.find(controller).path);
            Method met = c.getMethod(method, Map.class);
            Constructor constructor = c.getDeclaredConstructor(ApplicationContext.class);
            //这里每次都new，是否有问题，需不需要放到map里
            //JSONObject data = (JSONObject) met.invoke(constructor.newInstance(appContext), params);
            if (handlerMap.get(controller) == null)
            {
                handlerMap.put(controller, constructor.newInstance(appContext));
            }
            params.put("channelId", channelid);

            result = (ResponseSet) met.invoke(handlerMap.get(controller), params);
        }
        catch (Exception e)
        {
            log.error("invoke failed|controller={}|method={}|channelId={}|params={}", controller, method, channelid,
                params, e);
            ResponseValue value = new ResponseValue<>();
            value.set(ProtocolEnum.iq, ResponseEnum.NO_SUCH_CONTROLLER_METHOD);
            result.setValue(value);
        }
        return result;
    }

}
