package com.loovee.msgserver;

import com.alibaba.druid.pool.DruidDataSource;
import com.loovee.game.consts.SystemConst;
import com.loovee.game.message.bean.ChannelSession;
import com.loovee.msgserver.config.DataSource;
import com.loovee.msgserver.handle.SecureMsgServerHandler;
import com.loovee.msgserver.handle.SecureMsgServerInit;
import com.loovee.redis.connection.RedisConnection;
import com.xxx.rpc.client.RpcProxy;
import com.xxx.rpc.registry.zookeeper.ZooKeeperServiceDiscovery;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.ssl.util.SelfSignedCertificate;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.transaction.support.TransactionTemplate;
import redis.clients.jedis.ShardedJedis;

import java.io.IOException;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@SpringBootApplication(scanBasePackages = {"com.loovee.msgserver.config", "com.loovee.msgserver.jms", "com.loovee.msgserver.controller"})
@EnableAspectJAutoProxy
public class MsgServerApplication {
    @Value("${redis.hosts}")
    String host;
    @Value("${redis.maxTotal}")
    int maxTotal;
    @Value("${redis.maxWaitMillis}")
    long maxWaitMillis;
    @Value("${redis.maxIdle}")
    int maxIdle;
    @Value("${redis.testOnBorrow}")
    Boolean testOnBorrow;

    @Value("${rpc.registry_address}")
    String rpc_regAddr;

    @Autowired
    DataSource dataSource;

    public static int port = 8992;

    private static Logger log = LoggerFactory.getLogger(MsgServerApplication.class);

    /**
     * 获取redis链接
     *
     * @return
     */
    @Bean
    RedisConnection redisConnection() {
        RedisConnection connection = null;
        try {
            connection = new RedisConnection(host, maxTotal, maxWaitMillis, maxIdle, testOnBorrow);
        } catch (Exception e) {
            log.error("", e);
        }
        return connection;
    }

    @Bean
    public DruidDataSource druidDataSource() {
        DruidDataSource druidDataSource = new DruidDataSource();
        druidDataSource.setDriverClassName(dataSource.getDriverClassName());
        druidDataSource.setUrl(dataSource.getUrl());
        druidDataSource.setUsername(dataSource.getUsername());
        druidDataSource.setPassword(dataSource.getPassword());
        druidDataSource.setInitialSize(dataSource.getInitialSize());
        druidDataSource.setMaxActive(dataSource.getMaxActive());
        druidDataSource.setMaxWait(dataSource.getMaxWait());
        druidDataSource.setTimeBetweenEvictionRunsMillis(dataSource.getTimeBetweenEvictionRunsMillis());
        druidDataSource.setMinEvictableIdleTimeMillis(dataSource.getMinEvictableIdleTimeMillis());
        druidDataSource.setValidationQuery(dataSource.getValidationQuery());
        druidDataSource.setTestWhileIdle(dataSource.isTestWhileIdle());
        druidDataSource.setTestOnBorrow(dataSource.isTestOnBorrow());
        druidDataSource.setTestOnReturn(dataSource.isTestOnReturn());
        druidDataSource.setPoolPreparedStatements(dataSource.isPoolPreparedStatements());
        druidDataSource.setMaxPoolPreparedStatementPerConnectionSize(
                dataSource.getMaxPoolPreparedStatementPerConnectionSize());
        druidDataSource.setRemoveAbandonedTimeout(dataSource.getRemoveAbandonedTimeout());
        druidDataSource.setRemoveAbandoned(dataSource.isRemoveAbandoned());
        return druidDataSource;

    }

    @Bean
    public DataSourceTransactionManager dataSourceTransactionManager() {

        return new DataSourceTransactionManager(druidDataSource());

    }

    @Bean
    TransactionTemplate transactionTemplate() {
        TransactionTemplate template = new TransactionTemplate(dataSourceTransactionManager());
        template.setIsolationLevelName("ISOLATION_DEFAULT");
        template.setPropagationBehaviorName("PROPAGATION_REQUIRED");
        return template;

    }

    @Bean
    SqlSessionFactoryBean sqlSessionFactoryBean() {
        SqlSessionFactoryBean session = new SqlSessionFactoryBean();
        session.setDataSource(druidDataSource());
        ResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
        try {
            Resource[] resources = resolver.getResources("classpath*:/mybatis/mapper/*.xml");
            session.setMapperLocations(resources);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return session;

    }

    /**
     * 获得数据库连接
     *
     * @return
     * @throws Exception
     */
    @Bean
    SqlSessionTemplate sqlSessionTemplate() throws Exception {

        return new SqlSessionTemplate(sqlSessionFactoryBean().getObject());

    }

    @Bean
    ZooKeeperServiceDiscovery zooKeeperServiceDiscovery() {
        return new ZooKeeperServiceDiscovery(rpc_regAddr);
    }

    @Bean
    RpcProxy rpcProxy() {
        return new RpcProxy(zooKeeperServiceDiscovery());
    }


    public static void main(String[] args) throws Exception {
        ConfigurableApplicationContext context = SpringApplication.run(MsgServerApplication.class, args);

        // SelfSignedCertificate是一个用于管理可信消息的工厂管理者
        SelfSignedCertificate ssc = new SelfSignedCertificate();
        // 工厂；授权和发给私钥
//        SslContext sslCtx = SslContext.newServerContext(ssc.certificate(), ssc.privateKey());
        EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup();
        try {
            ServerBootstrap serverBootstrap = new ServerBootstrap();// 服务引导程序，服务器端快速启动程序
            serverBootstrap.group(bossGroup, workerGroup).channel(NioServerSocketChannel.class)
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new SecureMsgServerInit(null, context));

            if (null != args && args.length > 0 && args[0].matches("^[\\d]{4,5}")) {
                MsgServerApplication.port = Integer.parseInt(args[0]);
            }

            startSessionListener(context);

            log.info("netty im server started with port:{}", MsgServerApplication.port);
            serverBootstrap.bind(MsgServerApplication.port).sync().channel().closeFuture().sync();
        } catch (Throwable e) {
            log.error(e.getMessage());
        } finally {
            //jedis.close();
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
    }

    /**
     * 开启定时器(30s)
     * func:
     * 1.断开所有no_received
     * 2.将buffered移入no_received
     */
    private static void startSessionListener(ConfigurableApplicationContext context) {
        Runnable runnable = () -> {
            ShardedJedis jedis = null;
            try {
                jedis = context.getBean(RedisConnection.class).getSingletonInstance();
                String bufferedKey = SystemConst.SENDED_PING_CHANNEL_BUFFERED;
                String noReceivedKey = SystemConst.SENDED_NO_RECEIVED_CHANNEL;

                Set<String> noReceivedSet = jedis.smembers(noReceivedKey);
                log.info("clean no_received\n" + noReceivedSet.toString());
                if (noReceivedSet != null) {
                    for (String channelId : noReceivedSet) {
                        ChannelSession session = SecureMsgServerHandler.clients.get(channelId);
                        if (session != null) {
                            session.channel.close();
                            jedis.srem(noReceivedKey, channelId);
                        } else {
                            jedis.srem(noReceivedKey, channelId);
                        }
                    }
                }
                Set<String> bufferedSet = jedis.smembers(bufferedKey);
                log.info("merge buffered to no_received\n" + bufferedSet.toString());
                jedis.del(bufferedKey);
                for (String channelId : bufferedSet) {
                    jedis.sadd(noReceivedKey, channelId);
                }
            } catch (Exception e) {
                log.error("", e);
            } finally {
                jedis.close();
            }
        };
        ScheduledExecutorService service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(runnable, 10, 30, TimeUnit.SECONDS);
    }
}
