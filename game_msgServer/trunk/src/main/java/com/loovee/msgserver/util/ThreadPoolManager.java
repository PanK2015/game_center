package com.loovee.msgserver.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreadPoolManager
{

    private static volatile ThreadPoolManager instance;
    private static Logger log = LoggerFactory
        .getLogger(ThreadPoolManager.class);
    private ExecutorService pool;

    private ThreadPoolManager()
    {
        pool = Executors.newFixedThreadPool(50);
        //ThreadPoolExecutor p = (ThreadPoolExecutor)pool;
       // p.setCorePoolSize();

    }

    public ExecutorService getPool(){
        return pool;
    }
    
    public static ThreadPoolManager getInstance()
    {
        if (instance == null)
        {
            synchronized (ThreadPoolManager.class)
            {
                if (instance == null)
                {
                    instance = new ThreadPoolManager();
                }
            }
        }
        return instance;
    }

    public void execute(Runnable task)
    {
        try
        {
            pool.execute(task);
        }
        catch (Exception e)
        {
            log.error("Exception execute task,error=", e.getCause());
        }
    }

}
