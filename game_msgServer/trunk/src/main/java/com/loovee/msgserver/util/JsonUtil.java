package com.loovee.msgserver.util;

import com.alibaba.fastjson.JSONObject;

import java.util.Map;


/**
 * Created by loovee on 2016/9/21.
 */
public class JsonUtil
{
    public static <T> T MapToBean(Map<String, Object> map, Class<T> clazz)
    {
        String json = JSONObject.toJSONString(map);
        return JSONObject.parseObject(json, clazz);
    }

}
