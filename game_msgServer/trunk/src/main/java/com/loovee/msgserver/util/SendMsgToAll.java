package com.loovee.msgserver.util;

import com.loovee.msgserver.handle.SecureMsgServerHandler;

import java.util.Set;

/**
 * Created by loovee on 2016/9/29.
 */
public class SendMsgToAll implements Runnable
{
    private String msg;

    private Set<String> user_channels;

    public SendMsgToAll(String msg, Set<String> set)
    {
        this.msg = msg;
        this.user_channels = set;
    }

    @Override
    public void run()
    {

        for (String destChannelId : user_channels)
        {
            SecureMsgServerHandler.sendToChannel(destChannelId, msg);
        }

    }
}
