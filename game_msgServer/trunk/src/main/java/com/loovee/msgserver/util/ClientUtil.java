package com.loovee.msgserver.util;

import com.loovee.game.GameWebSocketClient;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/15
 */
public class ClientUtil {

    public static void sendWebSocketMsg(String host, int port, String field, String msg) {
        GameWebSocketClient webSocketClient = new GameWebSocketClient(host, port, field);
        webSocketClient.sendMsg(msg);
    }

}
