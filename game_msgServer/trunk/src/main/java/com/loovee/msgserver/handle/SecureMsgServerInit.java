package com.loovee.msgserver.handle;

import com.loovee.game.consts.SystemConst;
import com.loovee.game.message.bean.ChannelSession;
import com.loovee.msgserver.encoder.LineBasedFrameEncoder;
import com.loovee.redis.connection.RedisConnection;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.timeout.IdleStateHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.GenericFutureListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ConfigurableApplicationContext;
import redis.clients.jedis.ShardedJedis;

import java.nio.charset.Charset;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by loovee on 2016/9/19.
 */
public class SecureMsgServerInit extends ChannelInitializer<SocketChannel>
{
    private final SslContext ssl;
    ConfigurableApplicationContext context;
    Logger log = LoggerFactory.getLogger(SecureMsgServerInit.class);

    public SecureMsgServerInit(SslContext sslCtx,ConfigurableApplicationContext context) {
        this.ssl = sslCtx;
        this.context = context;
    }

    @Override
    protected void initChannel(SocketChannel ch) throws Exception
    {
        ChannelPipeline pi = ch.pipeline();
        //pi.addLast(ssl.newHandler(ch.alloc()));
        pi.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        pi.addLast(new StringDecoder());
        pi.addLast(new StringEncoder());
        // 设置空闲状态处理操作
        pi.addLast(new LineBasedFrameEncoder(Charset.forName("UTF-8"), 8192));
        pi.addLast("ping", new IdleStateHandler(60,30,90,TimeUnit.SECONDS));
        // and then business logic.
        pi.addLast(new SecureMsgServerHandler(context));
    }
}
