package com.loovee.msgserver.encoder;

import com.loovee.game.consts.MessageConst;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.handler.codec.TooLongFrameException;

import java.nio.charset.Charset;

/**
 * Created by china on 2016/9/26.
 */
public class LineBasedFrameEncoder extends MessageToByteEncoder<String> {

    private int maxLineLength = 8192;

    private Charset charset;

    public LineBasedFrameEncoder(){
        this(Charset.defaultCharset(), 8192);
    }

    public LineBasedFrameEncoder(Charset charset, int maxLineLength){
        this.charset = charset;
        this.maxLineLength = maxLineLength;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, String msg, ByteBuf out) throws Exception {
        if(msg == null){
            msg = "\n";
        } else {
            msg = msg.replaceAll("\r\n", MessageConst.LINE_REPLACE);
            msg = msg.replaceAll("\n", MessageConst.LINE_REPLACE);
            msg = msg.trim();
            msg += "\n";
        }
        ByteBufOutputStream bout = new ByteBufOutputStream(out);
        byte[] bytes = msg.getBytes(charset);
        if(bytes.length > maxLineLength){
            fail(ctx, bytes.length);
        } else {
            bout.write(bytes);
            bout.flush();
            bout.close();
        }
    }

    private void fail(final ChannelHandlerContext ctx, int length) {
        ctx.fireExceptionCaught(
                new TooLongFrameException(
                        "frame length (" + length + ") exceeds the allowed maximum (" + maxLineLength + ')'));
    }
}
