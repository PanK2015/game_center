package com.loovee.msgserver.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/14
 */
@Component
@ConfigurationProperties(prefix = "websocket", locations = "classpath:imClient.properties")
public class WebSocketClientSource {

    private String host;
    private int port;
    private String field;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }
}
