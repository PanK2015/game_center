package com.loovee.game.im;

import com.loovee.game.im.handler.SecureS2SClientInitializer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;

import javax.swing.*;

/**
 * Created by china on 2016/9/26.
 */
public class KeepAliveClient {

    private EventLoopGroup workerGroup;

    public int status = 0;

    private JTextArea area;

    private ChannelFuture future;

    public KeepAliveClient(JTextArea area){
        workerGroup = new NioEventLoopGroup();
        this.area = area;
    }

    public void connect(final String host, final int port) throws Exception {
        final SslContext sslctx = null;
        try {
            Bootstrap b = new Bootstrap();
            b.group(workerGroup);
            b.channel(NioSocketChannel.class);
            b.option(ChannelOption.SO_KEEPALIVE, true);

            b.handler(new ChannelInitializer<SocketChannel>() {
                @Override
                public void initChannel(SocketChannel ch) throws Exception {
                    ch.pipeline().addLast(new SecureS2SClientInitializer(sslctx, host, port, area));
                }
            });

            // Start the client.
            future = b.connect(host, port).sync();
            status = 1;
            // Wait until the connection is closed.
            //f.channel().closeFuture().sync();
        } finally {
//            workerGroup.shutdownGracefully();
//            status = 0;
        }

    }

    public void disconnect(){
        workerGroup.shutdownGracefully();
        status = 0;
    }

    public void sendMsg(String msg){
        future.channel().writeAndFlush(msg);
    }

}
