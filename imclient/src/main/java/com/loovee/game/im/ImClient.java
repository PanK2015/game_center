package com.loovee.game.im;

import com.loovee.game.im.enums.ConnectProtocolEnum;
import com.loovee.game.im.handler.SecureS2SClientInitializer;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by china on 2016/9/26.
 */
public class ImClient {
    public static int SSL_SENDING_SLEEP = 30;
    public static Channel channel = null;
    public EventLoopGroup clientGroup = null;
    private final String serverHost;
    private final int serverPort;
    private final String protocol;

    public ImClient(String HOST, int PORT, String protocol) {
        this.serverHost = HOST;
        this.serverPort = PORT;
        this.protocol = protocol;
    }

    public ImClient(String HOST, int PORT) {
        this.serverHost = HOST;
        this.serverPort = PORT;
        this.protocol = ConnectProtocolEnum.NOSSL.desc;
    }

    public ImClient(String hostPort) {
        String[] hostProtocol = null;
        if ((StringUtils.isNotBlank(hostPort)) && (hostPort.contains(":"))) {
            hostProtocol = hostPort.split(":");
        }
        this.serverHost = hostProtocol[0];
        this.serverPort = Integer.parseInt(hostProtocol[1]);
        if (hostProtocol.length > 2) {
            this.protocol = hostProtocol[2];
        } else {
            this.protocol = ConnectProtocolEnum.NOSSL.desc;
        }
    }

    public Channel connect() {
        SslContext sslctx = null;
        try {
            switch (ConnectProtocolEnum.find(this.protocol)) {
                case SSL:
                    sslctx = SslContext.newClientContext(InsecureTrustManagerFactory.INSTANCE);
                    break;
                case NOSSL:
                    break;
            }
            this.clientGroup = new NioEventLoopGroup();
            Bootstrap bootstrap = new Bootstrap();
            ((Bootstrap) ((Bootstrap) bootstrap.group(this.clientGroup)).channel(NioSocketChannel.class))
                    .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, 3000).handler(new SecureS2SClientInitializer(sslctx, this.serverHost, this.serverPort));
            channel = bootstrap.connect(this.serverHost, this.serverPort).sync().channel();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return channel;
    }

    public void sendMsg(String msg) {
        try {
            connect();
            channel.writeAndFlush(msg).sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                Thread.sleep(SSL_SENDING_SLEEP);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.clientGroup.shutdownGracefully();
        }
    }

    public void sendForceOffline(String msg) {
        try {
            connect();
            channel.writeAndFlush(msg).sync();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                Thread.sleep(SSL_SENDING_SLEEP);
                channel.close().sync();
            } catch (InterruptedException e) {
                e.printStackTrace();
            } finally {
                this.clientGroup.shutdownGracefully();
            }
        }
    }

    public void simulatorSendMsg(String msg) {
        ChannelFuture lastWriteFuture = null;
        try {
            if (channel == null) {
                channel = connect();
            }
            if (StringUtils.isNotBlank(msg)) {
                lastWriteFuture = channel.writeAndFlush(msg);
            }
            if (lastWriteFuture != null) {
                lastWriteFuture.sync();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        ImClient client = new ImClient("220.231.193.79", 8992);
//        client.sendMsg("test++++++++++++++++++++++++");
//    }
}
