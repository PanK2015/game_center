package com.loovee.game.im.frame;

import com.loovee.game.im.KeepAliveClient;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class ClientFrame {
	public static void main(String[] args) {
		ManageView();
	}

	private static KeepAliveClient client;

	public static void ManageView() {
		JFrame jframe = new JFrame("IM CLIENT");
		Container contentPane = jframe.getContentPane();
		contentPane.setLayout(new FlowLayout());

		final JTextField text = new JTextField(20);
		text.setText("192.168.20.125:8992");
		JButton connect = new JButton("连接");
		JButton close = new JButton("断开");
		JButton minaSend = new JButton("发送");
		
		final JTextArea minaSendArea = new JTextArea(6,60);
		JScrollPane scMinaSendPane = new JScrollPane(minaSendArea);
		scMinaSendPane.setBorder(BorderFactory.createTitledBorder("长连接发送"));
		
		final JTextArea area = new JTextArea(20, 60);
		JScrollPane scPan = new JScrollPane(area);
		scPan.setBorder(BorderFactory.createTitledBorder("长连接接收"));

		connect.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				String hostPort = text.getText();
				String host = null;
				int port = 0;
				try {
					host = hostPort.split(":")[0].trim();
					port = Integer.parseInt(hostPort.split(":")[1].trim());
				} catch (Exception e1) {
					area.append("参数错误\n");
					return;
				}
				try {
					client = new KeepAliveClient(area);
					client.connect(host, port);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		});

		close.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				client.disconnect();
				client = null;
				area.append("连接关闭\n");
			}
		});

		minaSend.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				if(client != null && client.status > 0){
					area.append("send ==> " + minaSendArea.getText() + "\n");
					client.sendMsg(minaSendArea.getText());
				} else {
					area.append("未连接服务器 \n");
				}
			}
		});
		JButton clearButton = new JButton("clear");
		clearButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				area.setText("");
			}
		});

		contentPane.add(text);
		contentPane.add(connect);
		contentPane.add(close);
		contentPane.add(minaSend);
		contentPane.add(scMinaSendPane);
		contentPane.add(scPan);
		contentPane.add(clearButton);

		jframe.setSize(700, 580);
		jframe.setLocation(300, 200);
		jframe.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				System.exit(0);
			}
		});
		jframe.setVisible(true);
	}
}
