package com.loovee.game.im.enums;

/**
 * Created by china on 2016/9/26.
 */
public enum ConnectProtocolEnum {
    SSL("ssl"),
    NOSSL("nossl"),
    TLS("tls");

    public String desc;

    ConnectProtocolEnum(String desc) {
        this.desc = desc;
    }

    public static ConnectProtocolEnum find(String desc){
        for(ConnectProtocolEnum type:ConnectProtocolEnum.values()){
            if(type.desc.equals(desc)){
                return type;
            }
        }
        return null;
    }
}
