/*
 * ��Ҫ����ͻ��˵ĳ�ʼ��
 */
package com.loovee.game.im.handler;

import com.loovee.game.im.encoder.LineBasedFrameEncoder;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.LengthFieldBasedFrameDecoder;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslContext;

import javax.swing.*;
import java.nio.charset.Charset;

/**
 * Creates a newly configured {@link ChannelPipeline} for a new channel.
 */
public class SecureS2SClientInitializer extends ChannelInitializer<SocketChannel> {

	String HOST;

	int PORT;

	private final SslContext sslCtx;

	private JTextArea area;

	public SecureS2SClientInitializer(SslContext sslCtx, String host, int port, JTextArea area) {
		this.sslCtx = sslCtx;
		this.HOST = host;
		this.PORT = port;
		this.area = area;
	}

	public SecureS2SClientInitializer(SslContext sslCtx, String host, int port) {
		this.sslCtx = sslCtx;
		this.HOST = host;
		this.PORT = port;
	}

	@Override
	public void initChannel(SocketChannel ch) throws Exception {
		ChannelPipeline pipeline = ch.pipeline();

		// Add SSL handler first to encrypt and decrypt everything.
		// In this example, we use a bogus certificate in the server side
		// and accept any invalid certificates in the client side.
		// You will need something more complicated to identify both
		// and server in the real world.
		if (sslCtx != null) {
			pipeline.addLast(sslCtx.newHandler(ch.alloc(), HOST, PORT));
		}
		// On top of the SSL handler, add the text line codec.
		pipeline.addLast(new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
		pipeline.addLast(new StringDecoder());
		pipeline.addLast(new StringEncoder());
		// 设置空闲状态处理操作
		pipeline.addLast(new LineBasedFrameEncoder(Charset.forName("UTF-8"), 8192));
		// 设置空闲状态处理操作
		// pipeline.addLast("ping", new IdleStateHandler(180, 180,
		// 60,TimeUnit.SECONDS));
		// and then business logic.
		pipeline.addLast(new SecureS2SClientHandler(area));
	}
}
