package com.loovee.game;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/17
 */
public class ExecuteClose {

    public static void main(String[] args) {
        if("msg".equals(args[0])) {
            MsgServerClose close = new MsgServerClose();
            close.run();
        } else if("websocketMsg".equals(args[0])) {
            MsgWebsocketClose close = new MsgWebsocketClose();
            close.run();
        } else {
            throw new NullPointerException("未传参数");
        }
    }

}
