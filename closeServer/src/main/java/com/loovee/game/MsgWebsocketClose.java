package com.loovee.game;

import com.alibaba.fastjson.JSONObject;
import com.loovee.game.consts.UserConst;
import com.loovee.redis.connection.RedisConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import redis.clients.jedis.ShardedJedis;

import javax.jms.*;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/15
 */
public class MsgWebsocketClose {

    private String redisHosts;
    private int maxTotal;
    private long maxWaitMillis;
    private int maxIdle;
    private boolean testOnBorrow;
    private ResourceBundle resourceBundle;
    private RedisConnection redisConnection;

    private String mqBrokeUrl;
    private String mqUsername;
    private String mqPassword;
    private ConnectionFactory connectionFactory;
    private Connection connection = null;
    private Session session;
    private Destination destination;
    private MessageProducer producer;

    public MsgWebsocketClose(){
        resourceBundle = ResourceBundle.getBundle("application");
        redisHosts = resourceBundle.getString("redis.hosts");
        maxTotal = Integer.parseInt(resourceBundle.getString("redis.maxTotal"));
        maxWaitMillis = Long.parseLong(resourceBundle.getString("redis.maxWaitMillis"));
        maxIdle = Integer.parseInt(resourceBundle.getString("redis.maxIdle"));
        testOnBorrow = Boolean.parseBoolean(resourceBundle.getString("redis.testOnBorrow"));
        mqBrokeUrl = resourceBundle.getString("spring.activemq.broker-url");
        mqUsername = resourceBundle.getString("spring.activemq.user");
        mqPassword = resourceBundle.getString("spring.activemq.password");
        try {
            redisConnection = new RedisConnection(redisHosts, maxTotal, maxWaitMillis, maxIdle, testOnBorrow);
            connectionFactory = new ActiveMQConnectionFactory(mqUsername, mqPassword, mqBrokeUrl);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        ShardedJedis jedis = null;
        try{
            jedis = redisConnection.getSingletonInstance();
            Set<String> set = jedis.smembers(UserConst.USER_WEB_ONLINE_SET);

            connection = connectionFactory.createConnection();
            connection.start();
            session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
            destination = session.createQueue("room.disconnect.clear");
            producer = session.createProducer(destination);
            producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

            for(String userId : set) {
                Map<String, Object> map = new HashMap<>();
                map.put("type", "disconnectClear");
                map.put("userId", userId);
                sendMessage(session, producer, JSONObject.toJSONString(map).toString());
            }

            session.commit();
            System.out.println("结束");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != connection)
                    connection.close();
            } catch (Throwable ignore) {}
        }
    }

    private void sendMessage(Session session, MessageProducer producer, String msg)
            throws Exception {
        TextMessage message = session.createTextMessage(msg);
        producer.send(message);
    }

}
