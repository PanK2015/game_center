package com.loovee.game.statistic.bean;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/11/8
 */
public class GameCenterStatistic {

    public int gameId;

    public int roomId;

    public String userId;

    public String sType;

    public int dateline;

    public int isCorrect;

    public String userVersion = "0";

    public int ymd;

}
