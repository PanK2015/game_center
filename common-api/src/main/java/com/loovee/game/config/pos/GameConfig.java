package com.loovee.game.config.pos;

/**
 * Created by china on 2016/9/28.
 */
public class GameConfig implements Comparable<GameConfig>{

    public int id;

    public int rank;

    public String gamename;

    public String icon;

    public String description;

    public String version;

    @Override
    public int compareTo(GameConfig o) {
        if(o.rank > this.rank){
            return 1;
        } else {
            return -1;
        }
    }
}
