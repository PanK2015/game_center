package com.loovee.game.guessSong.service;

import java.util.List;
import java.util.Map;

/**
 * Created by loovee on 2016/10/26.
 */
public interface GuessSongService
{
    public int ready(String roomId,String userid);

    public Map<String, Object> changeSong(String userId, List<Integer> currentWords);
}
