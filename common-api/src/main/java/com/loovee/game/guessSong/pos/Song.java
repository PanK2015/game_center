package com.loovee.game.guessSong.pos;

/**
 * Created by loovee on 2016/10/25.
 */
public class Song
{
    private int id;
    private String songName;
    private  String singer;
    private String lyric;

    public int getId()
    {
        return id;
    }

    public String getSongName()
    {
        return songName;
    }

    public String getSinger()
    {
        return singer;
    }

    public String getLyric()
    {
        return lyric;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setSongName(String songName)
    {
        this.songName = songName;
    }

    public void setSinger(String singer)
    {
        this.singer = singer;
    }

    public void setLyric(String lyric)
    {
        this.lyric = lyric;
    }
}
