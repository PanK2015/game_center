package com.loovee.game.adventure.pos;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/11
 */
public class AdventureResultItem implements Comparable<AdventureResultItem>{

    public String userId;

    public int random;

    public String nick;

    @Override
    public int compareTo(AdventureResultItem o) {
        if(this.random >= 0){
            return -1;
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdventureResultItem that = (AdventureResultItem) o;
        return userId.equals(that.userId);
    }

    @Override
    public int hashCode() {
        return userId.hashCode();
    }
}
