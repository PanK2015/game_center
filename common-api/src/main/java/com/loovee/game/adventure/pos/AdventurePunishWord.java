package com.loovee.game.adventure.pos;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/11
 */
public class AdventurePunishWord {

    public int punishId;
    public String punishWord;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AdventurePunishWord that = (AdventurePunishWord) o;

        return punishId == that.punishId;

    }

    @Override
    public int hashCode() {
        return punishId;
    }
}
