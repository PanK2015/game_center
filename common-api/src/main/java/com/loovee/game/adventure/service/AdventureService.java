package com.loovee.game.adventure.service;

import java.util.Map;

/**
 * Created by china on 2016/10/9.
 */
public interface AdventureService {

    /**
     * 准备
     * @param roomId
     * @param userId
     * @return
     */
    public int ready(String roomId, String userId);

    /**
     * 冒险
     * @param roomId
     * @param userId
     * @param random
     * @return
     */
    public Map<String, Object> adventure(String roomId, String userId, int random);

}
