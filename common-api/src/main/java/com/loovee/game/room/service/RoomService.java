package com.loovee.game.room.service;

import com.loovee.game.room.pos.RepoRoom;
import com.loovee.game.room.pos.TempRoom;
import com.loovee.game.user.bean.GameUser;

import java.util.List;
import java.util.Map;

/**
 * Created by loovee on 2016/9/26.
 */
public interface RoomService
{
    /**快速匹配*/
    public String matchingTempRoom(String gameId,String userId);

    /**查询房间信息*/
    public TempRoom queryTempRoom(String roomid);

    /**搜索房间*/
    public RepoRoom searchRoom(String roomid);

    /**我的房间*/
    public List<RepoRoom> myRoom(String useid);

    /** 进入房间 */
    public Map<String, Object> enterRoom(String roomId, String username);

    /**退出房间*/
    public int outRoom(String roomid,String userid);

    /**获取房间内用户资料*/
    public GameUser userInfo(String userId);

}
