package com.loovee.game.room.enums;

/**
 * Created by china on 2016/9/12.
 */
public enum RoomMethodEnum {

    search("search");

    private String method;

    RoomMethodEnum(String method){
        this.setMethod(method);
    }

    public java.lang.String getMethod() {
        return method;
    }

    public void setMethod(java.lang.String method) {
        this.method = method;
    }

    public static RoomMethodEnum findMethod(String method){
        for(RoomMethodEnum enumObj : RoomMethodEnum.values()){
            if(method.equals(enumObj.getMethod())){
                return enumObj;
            }
        }
        return null;
    }
}
