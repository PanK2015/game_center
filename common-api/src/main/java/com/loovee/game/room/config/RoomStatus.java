package com.loovee.game.room.config;

/**
 * 房间状态
 * Created by china on 2016/9/28.
 */
public class RoomStatus {

    /** 你画我猜 0准备阶段 */
    public static final int GUESS_PIC_STATUS_NONE = 0;

    /** 你画我猜 1准备完成未开始 */
    public static final int GUESS_PIC_STATUS_READY = 1;

    /** 你画我猜 2选词阶段 */
    public static final int GUESS_PIC_STATUS_SELECT = 2;

    /** 你画我猜 3游戏中 */
    public static final int GUESS_PIC_STATUS_GAMING = 3;

    /** 当前画画者结束 等待下一个人 */
    public static final int GUESS_PIC_STATUS_ROUND_END = 4;

    /*---------------------------------------------------------------------------------------*/

    /** 你唱我猜 0准备阶段 */
    public static final int GUESS_SONG_STATUS_NONE = 0;

    /** 你唱我猜 1准备完成未开始 */
    public static final int GUESS_SONG_STATUS_READY = 1;

    /** 你唱我猜 2选歌阶段 */
    public static final int GUESS_SONG_STATUS_SELECT = 2;

    /** 你唱我猜 3游戏中 */
    public static final int GUESS_SONG_STATUS_GAMING = 3;

    /** 当前画画者结束 等待下一个人 */
    public static final int GUESS_SONG_STATUS_ROUND_END = 4;
     /*---------------------------------------------------------------------------------------*/


    /** 房间状态：0 准备中 */
    public static final int ADVENTURE_STATUS_NONE = 0;

    /** 房间状态：1 准备完成未开始 */
    public static final int ADVENTURE_STATUS_READY = 1;

    /** 房间状态：2游戏中 */
    public static final int ADVENTURE_STATUS_GAMING = 2;

    /** 游戏结束 到 惩罚开始的结算 */
    public static final int ADVENTURE_STATUS_GAME_END = 3;

    /** 房间状态：3惩罚中 */
    public static final int ADVENTURE_STATUS_PUNISH = 4;

     /*---------------------------------------------------------------------------------------*/

    /** 房间状态 0 无状态 */
    public static final int TRUTH_STATUS_NONE = 0;

    /** 房间状态 1 可以提问 */
    public static final int TRUTH_STATUS_QUESTION = 1;

    /** 方将状态 2 回答问题 */
    public static final int TRUTH_STATUS_ANSWER = 2;

}
