package com.loovee.game.truth.service;

import java.util.Map;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/25
 */
public interface TruthService {

    public Map<String, Object> truth(String userId, String roomId);

}
