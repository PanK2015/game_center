package com.loovee.game.utils;

import com.loovee.game.consts.SystemConst;
import redis.clients.jedis.ShardedJedis;

/**
 * Created by pank on 2016/9/27.
 */
public class IdUtil {

    /**
     * 生成消息ID
     * @param channelId
     * @param jedis
     * @return
     */
    public synchronized static String generateId(String channelId, ShardedJedis jedis) {
        String key = SystemConst.CHANNEL_GENERATE_ID + channelId;
        if(jedis.exists(key)){
            String result = String.valueOf(jedis.incr(key));
            return channelId + "_" + result;
        } else {
            String result = "1";
            jedis.set(key, result);
            return channelId + "_" + result;
        }
    }

    /**
     * 删除消息ID
     * @param channelId
     * @param jedis
     * @return
     */
    public synchronized static void delId(String channelId, ShardedJedis jedis){
        String key = SystemConst.CHANNEL_GENERATE_ID + channelId;
        if(jedis.exists(key)){
            jedis.del(key);
        }
    }

}
