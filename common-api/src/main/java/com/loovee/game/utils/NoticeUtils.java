package com.loovee.game.utils;

import com.alibaba.fastjson.JSONObject;
import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.message.MessageTypeEnum;
import com.loovee.game.protocol.params.MessageSet;
import com.loovee.game.protocol.params.MessageValue;
import com.loovee.game.protocol.params.RequestSet;
import com.loovee.game.protocol.params.RequestValue;

import java.util.UUID;

/**
 * Created by loovee on 2016/9/29.
 */
public class NoticeUtils {

    /**
     * 组装消息下发
     * @param requestValue
     * @param roomId
     * @return
     */
    public static String dispatchMsg(RequestValue requestValue, String roomId, String exceptUsers) {
        RequestSet requestSet = new RequestSet();
        requestSet.setValue(requestValue);

        String id = UUID.randomUUID().toString().substring(16);
        requestSet.setId(id);
        requestSet.setValue(requestValue);

        MessageSet messageSet = new MessageSet();
        messageSet.setId(id);
        MessageValue<RequestSet> messageValue = new MessageValue<>();
        messageValue.set(MessageTypeEnum.normal, ProtocolEnum.message, roomId, requestSet, exceptUsers);

        messageSet.setValue(messageValue);

        return JSONObject.toJSONString(messageSet);

    }


}
