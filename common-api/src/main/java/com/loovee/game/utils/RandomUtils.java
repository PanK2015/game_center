package com.loovee.game.utils;

import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 * 随机数工具，单例模式
 * 
 * @author XuJijun
 *
 */
public class RandomUtils {
	private static Random random;

	// 双重校验锁获取一个Random单例
	public static Random getRandom() {
		if (random == null) {
			synchronized (RandomUtils.class) {
				if (random == null) {
					random = new Random();
				}
			}
		}

		return random;
	}

	/**
	 * 获得一个[0,max)之间的整数。
	 * 
	 * @param max
	 * @return
	 */
	public static int getRandomInt(int max) {
		return Math.abs(getRandom().nextInt()) % max;
	}

	/**
	 * 获得一个[0,max)之间的整数。
	 * 
	 * @param max
	 * @return
	 */
	public static long getRandomLong(long max) {
		return Math.abs(getRandom().nextInt()) % max;
	}

	/**
	 * 从list中随机取得一个元素
	 * 
	 * @param list
	 * @return
	 * @return
	 */
	public static Object getRandomElement(List<Object> list) {
		return list.get(getRandomInt(list.size()));
	}
	
	
	public static Object getRandomElementI(List<Integer> subIndexIdList) {
		// TODO Auto-generated method stub
		return subIndexIdList.get(getRandomInt(subIndexIdList.size()));
	}

	/**
	 * 从set中随机取得一个元素
	 * 
	 * @param set
	 * @return
	 */
	public static Object getRandomElement(Set<Object> set) {
		if (set.size() == 0) {
			return null;
		}
		int rn = getRandomInt(set.size());
		int i = 0;
		for (Object e : set) {
			if (i == rn) {
				return e;
			}
			i++;
		}
		return null;
	}
	
	/**
	 * 从set中随机取得一个元素
	 * @param set
	 * @return
	 */
	public static Object getRandomElementSet(Set set){
		if(set.size()==0){
			return null;
		}
		int rn = getRandomInt(set.size());
		int i = 0;
		for (Object e : set) {
			if(i==rn){
				return e;
			}
			i++;
		}
		return null;
	}

	/**
	 * 从map中随机取得一个key
	 * 
	 * @param map
	 * @return
	 */
	public static Object getRandomKeyFromMap(Map map) {
		int rn = getRandomInt(map.size());
		int i = 0;
		for (Object key : map.keySet()) {
			if (i == rn) {
				return key;
			}
			i++;
		}
		return null;
	}

	/**
	 * 从map中随机取得一个value
	 * 
	 * @param map
	 * @return
	 */
	public static Object getRandomValueFromMap(Map map) {
		int rn = getRandomInt(map.size());
		int i = 0;
		for (Object value : map.values()) {
			if (i == rn) {
				return value;
			}
			i++;
		}
		return null;
	}

	public static void main(String[] args) {
		int count = 0;
		for (int i = 0; i < 1000; i++) {
			System.out.println(Math.random());
			int odds = Math.random() * 10 > 2 ? 1 : 0;
			if (odds == 0)
				count++;
//			System.out.println(odds);
		}
		System.out.println(count);
	}



}
