package com.loovee.game.utils;

import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

@SuppressWarnings("restriction")
public class DESCBC {

	public static String encryptDES(String encryptString, String encryptKey) throws Exception {
		// IvParameterSpec zeroIv = new IvParameterSpec(new byte[8]);
		IvParameterSpec zeroIv = new IvParameterSpec(encryptKey.getBytes());
		SecretKeySpec key = new SecretKeySpec(encryptKey.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
		byte[] encryptedData = cipher.doFinal(encryptString.getBytes());
		return Base64.encode(encryptedData);
	}

	public static String decryptDES(String decryptString, String decryptKey) throws Exception {
		byte[] byteMi = Base64.decode(decryptString);
		IvParameterSpec zeroIv = new IvParameterSpec(decryptKey.getBytes());
		// IvParameterSpec zeroIv = new IvParameterSpec(new byte[8]);
		SecretKeySpec key = new SecretKeySpec(decryptKey.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.DECRYPT_MODE, key, zeroIv);
		byte decryptedData[] = cipher.doFinal(byteMi);
		return new String(decryptedData);
	}

	public static String bytesToHexString(byte[] src) {
		StringBuilder stringBuilder = new StringBuilder();
		if (src == null || src.length <= 0) {
			return null;
		}
		for (int i = 0; i < src.length; i++) {
			int v = src[i] & 0xFF;
			String hv = Integer.toHexString(v);
			if (hv.length() < 2) {
				stringBuilder.append(0);
			}
			stringBuilder.append(hv);
		}
		return stringBuilder.toString();
	}

	/**
	 * 特殊密码hash 反转取一半
	 * 
	 * @param password
	 * @param encryptKey
	 * @return
	 * @throws Exception
	 */
	public static String encrypt_password(String password, String encryptKey) throws Exception {
		int len = password.length();
		int padSize = 8 - len % 8;
		for (int i = 0; i < padSize; i++) {
			password += padSize;
		}
		IvParameterSpec zeroIv = new IvParameterSpec(encryptKey.getBytes());
		SecretKeySpec key = new SecretKeySpec(encryptKey.getBytes(), "DES");
		Cipher cipher = Cipher.getInstance("DES/CBC/PKCS5Padding");
		cipher.init(Cipher.ENCRYPT_MODE, key, zeroIv);
		byte[] encryptedData = cipher.doFinal(password.getBytes());
		return bytesToHexString(encryptedData);
	}

	public static String user_pass(String password) {
		try {
			password = encrypt_password(password, "1234abcd");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return password.substring(0, password.length() / 2);
	}

	public static void main(String[] args) throws Exception {
		// System.out.println(encryptDES("133665599888,20121000001","moblover"));
		// System.out.println(DES.encrypt("203204",
		// "s@x$cp.j")+"\t"+encryptDES("203204","s@x$cp.j"));
		// System.out.println(encryptDES("321321123","s@x$cp.j"));
		System.out.println(decryptDES("ubiuSRMsyCCyxtTHVxyA5w==", "s@x$cp.j"));
	}

}
