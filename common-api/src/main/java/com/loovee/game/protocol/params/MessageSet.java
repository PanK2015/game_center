package com.loovee.game.protocol.params;

/**
 * Created by loovee on 2016/9/29.
 */
public class MessageSet
{
    private String id;
    private MessageValue value;

    public void setId(String id)
    {
        this.id = id;
    }

    public void setValue(MessageValue value)
    {
        this.value = value;
    }

    public String getId()
    {
        return id;
    }

    public MessageValue getValue()
    {
        return value;
    }
}
