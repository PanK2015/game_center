package com.loovee.game.protocol.params;

import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.iq.ControllerEnum;
import com.loovee.game.protocol.enums.message.MessageTypeEnum;

import java.util.Map;

/**
 * Created by china on 2016/9/22.
 */
public class RequestValue {
    private ProtocolEnum protocol;

    private ControllerEnum controller;

    private String method;

    private Map<String, Object> params;

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public Map<String, Object> getParams() {
        return params;
    }

    public void setParams(Map<String, Object> params) {
        this.params = params;
    }

    public ProtocolEnum getProtocol() {
        return protocol;
    }

    public void setProtocol(ProtocolEnum protocol) {
        this.protocol = protocol;
    }

    public ControllerEnum getController() {
        return controller;
    }

    public void setController(ControllerEnum controller) {
        this.controller = controller;
    }
}
