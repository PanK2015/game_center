package com.loovee.game.protocol.params;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/20
 */
public class StreamSet {

    private String id;

    private StreamValue value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public StreamValue getValue() {
        return value;
    }

    public void setValue(StreamValue value) {
        this.value = value;
    }
}
