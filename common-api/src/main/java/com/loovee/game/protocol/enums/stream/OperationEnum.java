package com.loovee.game.protocol.enums.stream;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/20
 */
public enum OperationEnum {

    open("open"),
    close("close");

    public String op;

    OperationEnum(String op) {
        this.op = op;
    }

    public static OperationEnum indexOf(String op) {
        for (OperationEnum type : OperationEnum.values()) {
            if (type.op.equals(op)) {
                return type;
            }
        }
        return null;
    }

}
