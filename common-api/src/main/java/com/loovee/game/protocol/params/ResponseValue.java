package com.loovee.game.protocol.params;

import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.ResponseEnum;

/**
 * Created by loovee on 2016/9/21.
 */
public class ResponseValue<T>
{

    public ProtocolEnum protocol;

    public int code;

    public String msg;

    public T data;

    public String controller;

    public String method;

    public ResponseValue() {
        super();
    }

    public ResponseValue set(ProtocolEnum protocol, ResponseEnum responseEnum, String msg) {
        this.protocol = protocol;
        this.code = responseEnum.getCode();
        this.msg = msg;
        return this;
    }

    public ResponseValue set(ProtocolEnum protocol, ResponseEnum responseEnum){
        this.protocol = protocol;
        this.code = responseEnum.getCode();
        this.msg = responseEnum.getMsg();
        return this;
    }

    public ResponseValue set(ProtocolEnum protocol, ResponseEnum responseEnum, T data){
        this.protocol = protocol;
        this.code = responseEnum.getCode();
        this.msg = responseEnum.getMsg();
        this.data = data;
        return this;
    }

    public ResponseValue set(ProtocolEnum protocol, ResponseEnum responseEnum, String controller, String method) {
        this.protocol = protocol;
        this.code = responseEnum.getCode();
        this.msg = responseEnum.getMsg();
        return this;
    }

    public ResponseValue<T> set(ProtocolEnum protocol, ResponseEnum responseEnum, String controller, String method, T data){
        this.data = data;
        this.set(protocol, responseEnum, controller, method);
        return this;
    }
}
