package com.loovee.game.protocol.enums;

/**
 * Created by china on 2016/9/12.
 */
public enum ProtocolEnum {
    iq(1,"iq"),
    message(2,"message"),
    stream(3,"stream"),
    error(4,"error")
    ;
    public String descript;

    public int index;


    ProtocolEnum(int index, String name){
        this.index=index;
        this.descript=name;
    }


    public String getDescript() {
        return descript;
    }


    public int getIndex() {
        return index;
    }

    public static ProtocolEnum indexOf(int index){
        for(ProtocolEnum type: ProtocolEnum.values()){
            if(type.index==index){
                return type;
            }
        }
        return null;
    }


    public static ProtocolEnum find(String descript){
        for(ProtocolEnum type: ProtocolEnum.values()){
            if(type.descript.equalsIgnoreCase(descript) || type.name().equalsIgnoreCase(descript)){
                return type;
            }
        }
        return null;
    }
}
