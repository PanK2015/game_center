package com.loovee.game.protocol.params;

/**
 * Created by china on 2016/9/12.
 */
public class RequestSet {

    private String id;
    private RequestValue value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public RequestValue getValue() {
        return value;
    }

    public void setValue(RequestValue value) {
        this.value = value;
    }
}
