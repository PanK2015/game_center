package com.loovee.game.protocol.enums.iq;

/**
 * Created by china on 2016/9/12.
 */
public enum ControllerEnum {

    userController("userController", "com.loovee.msgserver.controller.UserController", "com.loovee.game_message.controller.UserController"),
    roomController("roomController", "com.loovee.msgserver.controller.RoomController", "com.loovee.game_message.controller.RoomController"),
    adventController("adventController", "com.loovee.msgserver.controller.AdventureController", "com.loovee.game_message.controller.AdventureController"),
    guessPicController("guessPicController", "com.loovee.msgserver.controller.GuessPicController", "com.loovee.game_message.controller.GuessPicController"),
    pingController("pingController","com.loovee.msgserver.controller.PingController", "com.loovee.game_message.controller.PingController"),
    guessSongController("guessSongController","com.loovee.msgserver.controller.GuessSongController","com.loovee.game_message.controller.GuessSongController"),
    truthController("truthController","com.loovee.msgserver.controller.TruthController","com.loovee.game_message.controller.TruthController");

    public String controller;
    public String path;
    public String websocketPath;

    ControllerEnum(String controller, String path, String websocketPath){
        this.controller = controller;
        this.path = path;
        this.websocketPath = websocketPath;
    }

    public static ControllerEnum find(String controller){
        for(ControllerEnum enumObj : ControllerEnum.values()){
            if(enumObj.controller.equals(controller)){
                return enumObj;
            }
        }
        return null;
    }



}
