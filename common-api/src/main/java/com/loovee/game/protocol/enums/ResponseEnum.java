package com.loovee.game.protocol.enums;

/**
 * Created by china on 2016/9/12.
 */
public enum ResponseEnum {

    /** 成功 */
    SUCCESS(200,"success"),
    /** 请求参数异常,可在方法中重写msg,描述具体参数异常 */
    PARAM_INVALID(0,"param invalid"),
    /** 系统报错,重写msg 打印stackTrace */
    SYSTEM_ERROR(500,"system error"),
    /** 未登录进行其他操作 */
    UNLOGIN(201,"action with no auth"),
    /** 没有查询到该方法 */
    NO_SUCH_CONTROLLER_METHOD(202,"no such controller or method"),
    /** 无法查询到房间 */
    NO_ROOM(203, "no room"),
    /**没有查询到该游戏*/
    NO_SUCH_GAME(204,"no such game"),
    /**没有查询到该用户*/
    NO_SUCH_USER(205,"no such user"),
    /**房间已满*/
    NO_SPACE(206,"There is no space in the room"),
    /** 房间已经准备中且用户不在 ROOM_USER_GAMING 缓存中,无法进入.游戏开始后可以进入 */
    ROOM_IS_READY(207, "Room is ready,can not enter"),
    /** 重复冒险 */
    ADVENTURE_REPEAT(208, "Can not adventure repeat"),
    /** 对面消费失败 */
    APP_PURCHASE_FAILED(209, "APP user purchase failed"),
    /** 用户已经进入过其他房间 */
    ALRADY_ENTERED_OTHER_ROOM(210, "User already entered other room"),
    /** 不能进入相同的房间 */
    CAN_NOT_ENTER_SAME_ROOM(211, "Can not enter same room"),
    /** 已经有人提问 */
    OTHER_ALREADY_TRUTH(212, "some one already asked"),
    /** 用户已经在线 */
    USER_ALREADY_LOGIN(300,"Cannot login(already in online list)");

    // TO DO: add every ERROR code

    private int code;

    private String msg;

    ResponseEnum(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static ResponseEnum find(int code){
        for(ResponseEnum e : ResponseEnum.values()){
            if(e.getCode() == code){
                return e;
            }
        }
        return null;
    }
}
