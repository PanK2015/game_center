package com.loovee.game.protocol.params;

import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.stream.OperationEnum;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/20
 */
public class StreamValue {

    private ProtocolEnum protocol;

    private String channelId;

    private String sessionId;

    private OperationEnum operation;

    public ProtocolEnum getProtocol() {
        return protocol;
    }

    public void setProtocol(ProtocolEnum protocol) {
        this.protocol = protocol;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public OperationEnum getOperation() {
        return operation;
    }

    public void setOperation(OperationEnum operation) {
        this.operation = operation;
    }
}
