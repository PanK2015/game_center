package com.loovee.game.protocol.params;

import com.loovee.game.protocol.enums.ProtocolEnum;
import com.loovee.game.protocol.enums.message.MessageTypeEnum;

/**
 * Created by loovee on 2016/9/29.
 */
public class MessageValue<T>
{
    public MessageTypeEnum messageType;

    public ProtocolEnum protocol;

    public String chatType;

    public String gameId;

    public String roomId;

    public String exceptUsers;

    public String destination;

    /** 是否要通过 socket-websocket */
    public String transfer = "false";

    public T message;


    public MessageValue set(MessageTypeEnum messageType,ProtocolEnum protocol,String chatType,String gameId,String roomId, String exceptUsers){
        this.messageType = messageType;
        this.protocol = protocol;
        this.chatType = chatType;
        this.gameId = gameId;
        this.roomId = roomId;
        this.exceptUsers = exceptUsers;
        return this;
    }

    public MessageValue set(MessageTypeEnum messageType, ProtocolEnum protocol, String roomId, T message, String exceptUsers){
        this.message = message;
        this.protocol = protocol;
        this.roomId = roomId;
        this.messageType = messageType;
        this.exceptUsers = exceptUsers;
        return this;
    }

    public MessageValue set(MessageTypeEnum messageType, ProtocolEnum protocol, String destination, String exceptUsers, T message){
        this.message = message;
        this.protocol = protocol;
        this.destination = destination;
        this.messageType = messageType;
        this.exceptUsers = exceptUsers;
        return this;
    }

}
