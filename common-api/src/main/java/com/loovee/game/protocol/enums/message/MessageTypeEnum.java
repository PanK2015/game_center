package com.loovee.game.protocol.enums.message;

public enum MessageTypeEnum {
	
	normal("normal"),
	chat("chat"),
	groupchat("groupchat"),
	headline("headline"),
	error("error")
	;
	
	public String descript;
	MessageTypeEnum(String descript){
		this.descript = descript;
	}
	
	public static MessageTypeEnum indexOf(String descript){
		for(MessageTypeEnum type:MessageTypeEnum.values()){
			if(type.name().equalsIgnoreCase(descript) || type.descript.equals(descript)){
				return type;
			}
		}
		return null;
	}
}
