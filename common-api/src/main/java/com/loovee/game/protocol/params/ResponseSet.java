package com.loovee.game.protocol.params;

/**
 * Created by china on 2016/9/22.
 */
public class ResponseSet {

    private String id;
    private ResponseValue value;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public ResponseValue getValue() {
        return value;
    }

    public void setValue(ResponseValue value) {
        this.value = value;
    }
}
