package com.loovee.game.user.requests;

/**
 * Created by china on 2016/9/12.
 */
public class LoginUserRequestParams extends UserRequestParams {

    private String appName;

    private String appUserId;

    private String appPassword;

    private String  channelId;

    private String clientType;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    private String version;

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getAppName() {
        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getAppUserId() {
        return appUserId;
    }

    public void setAppUserId(String appUserId) {
        this.appUserId = appUserId;
    }

    public String getAppPassword() {
        return appPassword;
    }

    public void setAppPassword(String appPassword) {
        this.appPassword = appPassword;
    }

    public void setChannelId(String channelId)
    {
        this.channelId = channelId;
    }

    public String getChannelId()
    {
        return channelId;
    }

    public LoginUserRequestParams() {
        super();
    }

    public LoginUserRequestParams(String appName, String appUserId, String appPassword,String channelId) {
        super();
        this.appName = appName;
        this.appUserId = appUserId;
        this.appPassword = appPassword;
        this.channelId = channelId;
    }
}
