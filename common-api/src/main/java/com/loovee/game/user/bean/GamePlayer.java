package com.loovee.game.user.bean;

/**
 * 房间内用户
 * Created by china on 2016/9/28.
 */
public class GamePlayer {

    public String userId;

    public String nick;

    public String avatar;

    public String gendar;

    public boolean isOwner = false;

    public int role;

    public int userNum;
}
