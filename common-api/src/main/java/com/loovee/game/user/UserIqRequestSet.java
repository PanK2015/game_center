package com.loovee.game.user;

import com.loovee.game.protocol.params.RequestSet;
import com.loovee.game.user.enums.UserMethodEnum;
import com.loovee.game.user.requests.UserRequestParams;

/**
 * Created by china on 2016/9/12.
 */
public class UserIqRequestSet extends RequestSet{

    private UserMethodEnum method;

    public UserRequestParams params;

    public UserRequestParams getParams() {
        return params;
    }

    public void setParams(UserRequestParams params) {
        this.params = params;
    }

    public UserMethodEnum getMethod() {
        return method;
    }

    public void setMethod(UserMethodEnum method) {
        this.method = method;
    }
}
