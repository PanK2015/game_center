package com.loovee.game.user.service;

import com.loovee.game.config.pos.GameConfig;
import com.loovee.game.user.bean.GameUser;
import com.loovee.game.user.bean.Gift;
import com.loovee.game.user.requests.LoginUserRequestParams;

import java.util.List;
import java.util.Map;

/**
 * Created by loovee on 2016/9/21.
 */
public interface UserService

{
    public Map<String, Object> login(LoginUserRequestParams params);

    public List<GameConfig> homepage(String userId);

    public Map<String,Object> inviteFriend(String userId,int start,int end);

    public List<Gift> getGifts();

    public int inviteMsg(String from_user,String recv_user,String roomId,String gameName);

    public int giftNotice(String giftId,String roomId,String userId);
}
