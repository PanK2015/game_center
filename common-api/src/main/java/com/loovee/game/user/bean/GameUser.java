package com.loovee.game.user.bean;

/**
 * Created by loovee on 2016/9/22.
 */
public class GameUser
{
    private String userId;
    private String nick;
    private String gender;
    private String avatar;
    private String age;
    private String sign;
    private String version = "";

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setNick(String nick)
    {
        this.nick = nick;
    }

    public void setGender(String gender)
    {
        this.gender = gender;
    }

    public void setAvatar(String avatar)
    {
        this.avatar = avatar;
    }

    public String getUserId()
    {
        return userId;
    }

    public void setUserId(String userId)
    {

        this.userId = userId;
    }

    public void setAge(String age)
    {
        this.age = age;
    }

    public void setSign(String sign)
    {
        this.sign = sign;
    }

    public String getNick()
    {
        return nick;
    }

    public String getGender()
    {
        return gender;
    }

    public String getAvatar()
    {
        return avatar;
    }

    public String getAge()
    {
        return age;
    }

    public String getSign()
    {
        return sign;
    }
}
