package com.loovee.game.user.enums;

/**
 * Created by china on 2016/9/12.
 */
public enum UserMethodEnum {

    login("login");

    private String method;

    UserMethodEnum(String method){
        this.setMethod(method);
    }

    public java.lang.String getMethod() {
        return method;
    }

    public void setMethod(java.lang.String method) {
        this.method = method;
    }

    public static UserMethodEnum findMethod(String method){
        for(UserMethodEnum enumObj : UserMethodEnum.values()){
            if(method.equals(enumObj.getMethod())){
                return enumObj;
            }
        }
        return null;
    }

}
