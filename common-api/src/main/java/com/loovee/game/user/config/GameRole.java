package com.loovee.game.user.config;

/**
 * 游戏中参加者的role
 * Created by china on 2016/9/28.
 */
public class GameRole {

    /** 游戏未开始时 */
    public static final int NONE = 0;

    /** 围观者 */
    public static final int AUDIENCE = 1;

    /** 参与者 */
    public static final int PLAYER = 2;

}
