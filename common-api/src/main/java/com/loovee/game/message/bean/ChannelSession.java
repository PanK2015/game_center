package com.loovee.game.message.bean;

import io.netty.channel.Channel;

/**
 * Created by loovee on 2016/9/20.
 */
public class ChannelSession
{
    public String username;

    public String serverAddress;

    public Channel channel;

}
