package com.loovee.game.message.bean;

import org.springframework.web.socket.WebSocketSession;

/**
 * @Description: web socket
 * @Author PanK
 * @Date 2016/10/11
 */
public class GameWebSocketSession {

    public String username;

    public String serverAddress;

    public WebSocketSession webSocketSession;
}
