package com.loovee.game.consts;

/**
 * Created by china on 2016/10/9.
 */
public class AdventureConst {

    /** 大冒险每个人丢出的点数 + roomId (userId-number) */
    public static final String ADVENTURE_RANDOM_HASH = "room_adventure_random_";

    /** 大冒险所有的惩罚 */
    public static final String ADVENTURE_PUNISH_WORD = "room_adventure_punish_word";

    /** 房间内准备即开始的人数 */
    public static final Integer ADVENTURE_READY_START_SIZE = 4;

    /**游戏id*/
    public static final String ADVENTURE_GAMEID = "2";

    /**法官头像*/
    public static final String FAGUAN_AVATAR = "leyuan_faguan_pic.png";

    /**游戏输家*/
    public static final String GAME_LOSERS = "game_losers";
}
