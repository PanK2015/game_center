package com.loovee.game.consts;

/**
 * Created by loovee on 2016/9/26.
 */
public class RoomConst
{
    /**房间内的玩家列表 set*/
    public static final String ROOM_USER = "room_user_";

    /** 房间内最大人数 */
    public static final Integer ROOM_MAX_SIZE = 8;

    /** 用户进入的房间 value */
    public static final String USER_ENTERED_ROOM = "user_entered_room_"; // + userId

    /**房间内正在游戏中的玩家 zset*/
    public static final String ROOM_USER_GAMING = "room_user_gaming_";

     /**房间信息，hash 包括房间名称 roomName，game_id, status */
    public static final String ROOM_INFO = "room_info_";

    /**在线房间列表 set*/
    public static final String ROOM_ONLINE = "room_online_";

    /**房间内相关的定时信息，
     * ready 4人准备
     * choose 开始选词
     * begin 开始
     * roundEnd 一轮结束
     * round 当前第几轮
     * word 当前选中的词
     * userId 当前出题人
     * */
    public  static final String GUESS_ROOM_TIME = "guess_room_time_";

    /**房间内相关的定时信息，
     * ready 4人准备
     * choose 开始选词
     * begin 开始
     * roundEnd 一轮结束
     * round 当前第几轮
     * word 当前选中的词
     * userId 当前出题人
     * */

    public  static final String GUESS_SONG_ROOM_TIME = "guess_song_room_time_";

    /**
     * 大冒险游戏过程中信息
     * ready  4人准备时间
     * begin  开始时间
     * gameEnd  掷骰子结束时间
     * punish  惩罚开始时间
     */
    public static final String ADVENTURE_ROOM_TIME = "adventure_room_time_";

    /**
     * 真心话时间轴
     * begin    问题下发时间(15s)
     * question 问题
     */
    public static final String TRUTH_ROOM_TIME = "truth_room_time_";

    /**
     * 一分钟内匹配过的房间号，list
     */
    public static final String USER_MATCHED_ROOM="user_matched_room_";

    /**
     * 用户匹配的房间，一分钟有效
     */
    public static final String USER_MATCHED_ROOM_EX = "user_matched_room_ex_";

}
