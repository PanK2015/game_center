package com.loovee.game.consts;

/**
 * Created by china on 2016/10/8.
 */
public class GuessPicConst {

    /** 保存所有的词 */
    public static final String GUESS_PIC_WORD_HASH = "guess_pic_word_hash";

    /** 每轮回答问题后 + roomId */
    public static final String GUESS_PIC_EVERY_ROUND_HASH = "guess_pic_every_round_";

    /**作画的消息缓存 list*/
    public static final String GUESS_PIC_MAKEPIC_CACHE = "guess_pic_makepic_cache_";

    /**作画信息缓存接收人占位符*/
    public static final String DESTINATION_HOLDER = "destination_holder_##";

    /** 房间内准备即开始的人数 */
    public static final Integer GUESSPIC_READY_START_SIZE = 4;

    /**当前待选的词语 +roomId*/
    public static final String GUESS_PIC_READY_WORD = "guess_pic_ready_word_";

    public static final String GUESS_PIC_GAMEID = "1";

    public static final String LAOSHI_AVATAR = "leyuan_laoshi_pic.png";

    /**回答的记录，list  +roomId*/
    public static final String ANSWER_PIC_RECORD = "answer_pic_record_";


}
