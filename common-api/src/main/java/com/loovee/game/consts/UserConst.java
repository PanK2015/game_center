package com.loovee.game.consts;

/**
 * Created by loovee on 2016/9/23.
 * 存放相关的缓存信息
 */
public class UserConst
{
    /**user和channelid的关联*/
    public static final String USERID_CHANNEL_PREFIX = "userid_channel_";

    /** user 和 sessionid 的关联 */
    public static final String USERID_SESSION_PREFIX = "user_id_session_";

    /** 用户通过APP登录 */
    public static final String USER_APP_ONLINE_SET = "user_app_online_set";

    /** 用户通过WEB登录 */
    public static final String USER_WEB_ONLINE_SET = "user_web_online_set";

}
