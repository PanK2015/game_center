package com.loovee.game.consts;

/**
 * @Description:
 * @Author PanK
 * @Date 2016/10/25
 */
public class TruthConst {

    /** 真心话问题库 hash */
    public static final String TRUTH_QUESTION_BANK = "truth_question_bank";

    /** 游戏开始所需要的人数 */
    public static final Integer TRUTH_BEGIN_SIZE = 2;

    /**游戏id*/
    public static final String TRUTH_GAME_ID = "3";

}
