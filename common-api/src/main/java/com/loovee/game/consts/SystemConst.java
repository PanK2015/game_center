package com.loovee.game.consts;

/**
 * Created by pank on 2016/9/27.
 */
public class SystemConst {

    /** 1.2分钟Idle时发送ping包,发送出后加入到缓冲池 */
    /** 1.每30s轮询一次(关闭 NO_RECEIVED 所有连接,将 BUFFERED 移入 NO_RECEIVED ) */
    /** 3.接收到返回时从 BUFFERED 和 NO_RECEIVED 中删除 */
    /** 已经发送出ping包的连接(缓冲池) */
    public static final String SENDED_PING_CHANNEL_BUFFERED = "sended_ping_channel_set";
    /** 已经发送出ping包的连接 */
    public static final String SENDED_NO_RECEIVED_CHANNEL = "sended_no_received_channel";

    /** + channelId (key-value) 服务器下发消息产生的ID */
    public static final String CHANNEL_GENERATE_ID = "channel_message_id_";
}
