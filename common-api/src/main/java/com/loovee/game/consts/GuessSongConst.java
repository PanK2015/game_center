package com.loovee.game.consts;

/**
 * Created by loovee on 2016/10/25.
 */
public class GuessSongConst
{
    /** 保存所有的词 */
    public static final String GUESS_SONG_WORD_HASH = "guess_song_song_hash";

    /** 每轮回答问题后 + roomId */
    public static final String GUESS_SONG_EVERY_ROUND_HASH = "guess_song_every_round_";

    /**录音消息缓存 list*/
    public static final String GUESS_SONG_MAKEPIC_CACHE = "guess_song_makepic_cache_";

    /**作画信息缓存接收人占位符*/
    public static final String DESTINATION_HOLDER_SONG = "destination_holder_%%##";

    /** 房间内准备即开始的人数 */
    public static final Integer GUESSSONG_READY_START_SIZE = 4;

    public static final String GUESS_SONG_GAMEID = "4";

    public static final String HOST_AVATAR = "leyuan_zhuchiren_pic@2x.png";

    public static final String HOST_NICK = "主持人";

    /**回答的记录，list  +roomId*/
    public static final String ANSWER_SONG_RECORD = "answer_song_record_";
}
