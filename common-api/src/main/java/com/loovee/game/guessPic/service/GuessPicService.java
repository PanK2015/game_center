package com.loovee.game.guessPic.service;

import java.util.List;
import java.util.Map;

/**
 * Created by loovee on 2016/9/29.
 */
public interface GuessPicService
{
    public int ready(String roomId,String userid);

    public Map<String, Object> changeWord(String userId, List<Integer> currentWords);

    public Map<String, Object> chooseWord(String roomId, String wordId);
}
