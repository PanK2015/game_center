package com.loovee.game.guessPic.pos;

/**
 * Created by china on 2016/9/30.
 */
public class PicWord {

    public int wordId;

    public String wordText;

    public String hint1;

    public String hint2;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PicWord word = (PicWord) o;

        return wordId == word.wordId;

    }

    @Override
    public int hashCode() {
        return wordId;
    }
}
