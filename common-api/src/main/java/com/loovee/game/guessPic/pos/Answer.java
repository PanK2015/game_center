package com.loovee.game.guessPic.pos;

/**
 * Created by china on 2016/10/8.
 */
public class Answer implements Comparable<Answer>{

    public int rank;
    public int score;
    public int total;
    public String userid;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Answer answer = (Answer) o;

        return userid.equals(answer.userid);

    }

    @Override
    public int hashCode() {
        return userid.hashCode();
    }

    @Override
    public int compareTo(Answer o) {
        if(this.total < o.total){
            return -1;
        } else {
            return 1;
        }
    }
}
